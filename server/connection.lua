require "common.base"
local socketlib = require "socket"
local json = require "3rdparty.json"

ENTITY_SYNC_INTERVAL = 10.0
OBJECT_SYNC_INTERVAL = 60.0

Connection = setmetatable({
  class_name = "Connection",
  server_ref = nil,
  tickrate = nil,
  ip = nil,
  port = nil,
  id = nil,
  token = nil,
  map_id = nil,
  entity_id = nil,
  ts_packet_out = nil,
  packets_in = nil,
  packets_out = nil,
  state = nil
}, {__index = Base})

function Connection:new(instance)
  instance = instance or {}
  setmetatable(instance, self)
  self.__index = self
  return instance:init()
end

function Connection:init()
  self.tickrate = ObjUtil.set_if_nil(self.tickrate, 1/15)
  self.packets_in = {}
  self.packets_out = {}
  self.state = ObjUtil.set_if_nil(self.state, {
    connected = true,
    ts_ping = socketlib.gettime(),
    ts_pong = socketlib.gettime(),
    latency_in_ms = 0.0,
    req_map_update = true,
    req_cell_update = true,
    ts_entity_update = socketlib.gettime() - ENTITY_SYNC_INTERVAL,
    ts_object_update = socketlib.gettime() - OBJECT_SYNC_INTERVAL,
  })

  return self
end

function Connection:update(dt)
  -- get current timestamp
  local ts = socketlib.gettime()

  -- disconnect if latency too high
  if ts - self.state.ts_pong > 10 then
    self.state.connected = false
    return
  end

  -- send ping at intervals
  if ts - self.state.ts_ping > 1.0 then
    self:add_packet_out(Packet:new{
      id = PROTOCOL_PACKETS.PING,
      data = {
        latency_in_ms = self.state.latency_in_ms
      }
    })
    self.state.ts_ping = ts
  end

  -- 1st: update map data
  if self.state.req_map_update == true then
    -- get map data
    local map_data = self.server_ref.maps[self.map_id]:export_data()
    -- construct packet
    self:add_packet_out(Packet:new{
      id = PROTOCOL_PACKETS.MAP_SYNC_RSP,
      client_id = self.id,
      data = {
        width = map_data.width,
        height = map_data.height,
        size = map_data.size,
        bbox = map_data.bbox
      }
    })
    self.state.req_map_update = false

  -- 2nd: update cell data
  elseif self.state.req_cell_update == true then
    -- get cell data
    local cell_data = self.server_ref.maps[self.map_id]:export_cell_data()
    -- construct packets
    local cell_data_buf = {}
    for c_k, c in pairs(cell_data) do
      table.insert(cell_data_buf, c)

      if #cell_data_buf >= 16 or next(cell_data, c_k) == nil then
        self:add_packet_out(Packet:new{
          id = PROTOCOL_PACKETS.CELL_SYNC_RSP,
          client_id = self.id,
          data = cell_data_buf
        })
        cell_data_buf = {}
      end
    end
    self.state.req_cell_update = false

  -- others
  else
    -- send entity update at intervals
    if ts - self.state.ts_entity_update > ENTITY_SYNC_INTERVAL then
      -- get entity data
      local entity_data = self.server_ref.maps[self.map_id]:export_entity_data()
      -- construct packets
      local entity_data_buf = {}
      for e_k, e in ipairs(entity_data) do
        table.insert(entity_data_buf, e)

        if #entity_data_buf >= 16 or next(entity_data, e_k) == nil then
          self:add_packet_out(Packet:new{
            id = PROTOCOL_PACKETS.ENTITY_SYNC_RSP,
            client_id = self.id,
            data = entity_data_buf
          })
          entity_data_buf = {}
        end
      end
      self.state.ts_entity_update = socketlib.gettime()
    end
    -- send object update at intervals
    if ts - self.state.ts_object_update > OBJECT_SYNC_INTERVAL then
      -- get object data
      local object_data = self.server_ref.maps[self.map_id]:export_object_data()
      -- construct packets
      local object_data_buf = {}
      for o_k, o in ipairs(object_data) do
        table.insert(object_data_buf, o)

        if #object_data_buf >= 16 or next(object_data, o_k) == nil then
          self:add_packet_out(Packet:new{
            id = PROTOCOL_PACKETS.OBJECT_SYNC_RSP,
            client_id = self.id,
            data = object_data_buf
          })
          object_data_buf = {}
        end
      end
      self.state.ts_object_update = socketlib.gettime()
    end
  end
end

function Connection:add_packet_in(packet)
  if packet == nil or packet.id == nil then return end

  table.insert(self.packets_in, packet)
end

function Connection:process_packets_in()
  -- get current timestamp
  local ts = socketlib.gettime()

  for _, packet in ipairs(self.packets_in) do
    -- authenticate sender
    if packet.token == nil or packet.token ~= self.token then
      print(string.format("process packets in - error, invalid token for connection ID %d!", self.id))
    else
      -- handle pong response
      if packet.id == PROTOCOL_PACKETS.PONG then
        self.state.ts_pong = ts
        self.state.latency_in_ms = math.floor(1000.0 * (self.state.ts_pong - self.state.ts_ping))

      -- handle message request
      elseif packet.id == PROTOCOL_PACKETS.MESSAGE_REQ then
        -- only proceed if message is valid
        if packet.data.message ~= nil and string.len(packet.data.message) > 0 and string.len(packet.data.message) < 128 then
          -- send message to all clients in the same map
          self.server_ref:add_packet_out(Packet:new{
            id = PROTOCOL_PACKETS.MESSAGE_RSP,
            map_id = self.map_id,
            data = {
              source = MESSAGE_SOURCES.client,
              sender = tostring(self.id),
              entity_id = self.entity_id,
              message = packet.data.message
            }
          })
        end

      -- handle entity move request
      elseif packet.id == PROTOCOL_PACKETS.ENTITY_MOVE_REQ then
        -- only proceed if connection owns the entity
        if self.entity_id == packet.data.id then
          -- get the entity
          local entity = self.server_ref.maps[self.map_id]:get_entity(packet.data.id)
          -- only proceed if the entity exists
          if entity ~= nil then
            entity:set_target_move(packet.data.x, packet.data.y)
          end
        end
      end
    end
  end

  self.packets_in = {}
end

function Connection:add_packet_out(packet)
  if packet == nil or packet.id == nil then return end

  table.insert(self.packets_out, packet)
end

function Connection:process_packets_out()
  -- get server socket and current timestamp
  local socket = self.server_ref.socket
  local ts = socketlib.gettime()

  -- send buffered packets
  local skipped_packets = {}
  for _, packet in ipairs(self.packets_out) do
      -- fixed tickrate
      local skip_packet = false
      if self.ts_packet_out ~= nil then
        local ts_diff = ts - self.ts_packet_out
        if ts_diff < self.tickrate then
          skip_packet = true
          table.insert(skipped_packets, packet)
        end
      end
      if skip_packet == false then
        socket:sendto(json.encode(packet), self.ip, self.port)
        self.ts_packet_out = socketlib.gettime()
      end
  end

  self.packets_out = skipped_packets

  -- check for buffer overflow
  local packets_out_len = #self.packets_out
  if packets_out_len > 100 then
    print(string.format("warning: client ID %d packets_out buffer size: %d", self.id, packets_out_len))
  end
end
