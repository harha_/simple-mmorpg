require "common.base"
require "common.math_util"
require "common.entity_data"
require "common.entity"
require "common.cell"

EntityAi = {
  ["slime"] = {
    execute = function(entity, dt)
      local roam_chance = MathUtil.randomf(0.0, 1.0)
      if entity.state == ENTITY_STATES.idle and roam_chance > 0.8 then
        local roam_r = entity.data.ai.roam_range
        local roam_x = MathUtil.randomf(entity.pos.x - roam_r, entity.pos.x + roam_r)
        local roam_y = MathUtil.randomf(entity.pos.y - roam_r, entity.pos.y + roam_r)
        local roam_c = entity.map_ref:get_cell_by_xy(roam_x, roam_y)

        if roam_c ~= nil and roam_c.flag ~= CELL_FLAGS.block then
          entity:set_target_path(roam_c.x, roam_c.y)
        end
      end
    end
  }
}
