require "common.base"
require "common.cell"
require "common.bbox"

Cell = setmetatable({
  class_name = "Cell",
  map_ref = nil,
  x = nil,
  y = nil,
  z = nil,
  bbox = nil,
  flag = nil,
  tiles = nil,
  phys = nil
}, {__index = Base})

function Cell:new(instance)
  instance = instance or {}
  setmetatable(instance, self)
  self.__index = self
  return instance:init()
end

function Cell:init()
  self.map_ref = self.map_ref
  self.x = ObjUtil.set_if_nil(self.x, 0)
  self.y = ObjUtil.set_if_nil(self.y, 0)
  self.z = ObjUtil.set_if_nil(self.z, 0)
  self.bbox = ObjUtil.set_if_nil(self.bbox, BBox:new{
    min_x = self.x,
    min_y = self.y,
    max_x = self.x + 1,
    max_y = self.y + 1
  })
  self.flag = ObjUtil.set_if_nil(self.flag, CELL_FLAGS.walk)
  self.tiles = ObjUtil.set_if_nil(self.tiles, {})
  self.phys = nil
  self:init_phys()

  return self
end

function Cell:init_phys()
  if self.phys ~= nil then
    self.phys.body:destroy()
    self.phys = nil
  end

  if self.flag == CELL_FLAGS.walk then return end

  self.phys = {
    body = love.physics.newBody(self.map_ref.phys.world, self.x + self.bbox.dim_x * 0.5, self.y + self.bbox.dim_y * 0.5, "static"),
    shape = love.physics.newRectangleShape(self.bbox.dim_x, self.bbox.dim_y),
    fixture = nil
  }
  self.phys.fixture = love.physics.newFixture(self.phys.body, self.phys.shape)
end

function Cell:export_data()
  local data = {
    x = self.x,
    y = self.y,
    z = self.z,
    flag = self.flag,
    tiles = {}
  }
  for _, t in pairs(self.tiles) do
    table.insert(data.tiles, t:export_data())
  end

  return data
end
