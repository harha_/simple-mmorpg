require "common.base"
require "common.bbox"
require "server.cell"
require "server.tile"
require "server.entity"
require "server.object"
local json = require "3rdparty.json"

Map = setmetatable({
  class_name = "Map",
  server_ref = nil,
  id = nil,
  file_path = nil,
  width = nil,
  height = nil,
  size = nil,
  bbox = nil,
  cells = nil,
  entities = nil,
  objects = nil,
  phys = nil
}, {__index = Base})

function Map:new(instance)
  instance = instance or {}
  setmetatable(instance, self)
  self.__index = self
  return instance:init()
end

function Map:init()
  self.file_path = ObjUtil.set_if_nil(self.file_path, "res/maps/default.json")
  self.width = ObjUtil.set_if_nil(self.width, 8)
  self.height = ObjUtil.set_if_nil(self.height, 8)
  self.size = self.width * self.height
  self.bbox = BBox:new{
    min_x = 0,
    min_y = 0,
    max_x = self.width,
    max_y = self.height
  }
  self.cells = {}
  self.entities = {}
  self.objects = {}
  self.phys = nil
  self:load()

  return self
end

function Map:init_phys()
  if self.phys ~= nil then
    self.phys.world:destroy()
    self.phys.world = nil
    self.phys.body = nil
    self.phys.shape = nil
    self.phys.fixture = nil
  end

  love.physics.setMeter(1.0)
  self.phys = {
    world = love.physics.newWorld(0, 0, true),
    body = nil,
    shape = nil,
    fixture = nil
  }
  self.phys.body = love.physics.newBody(self.phys.world, self.bbox.min_x, self.bbox.min_y, "static")
  self.phys.shape = love.physics.newChainShape(true, 0, 0, self.width, 0, self.width, self.height, 0, self.height)
  self.phys.fixture = love.physics.newFixture(self.phys.body, self.phys.shape)
end

function Map:export_data()
  local data = {
    width = self.width,
    height = self.height,
    size = self.size,
    bbox = {
      min_x = self.bbox.min_x,
      min_y = self.bbox.min_y,
      max_x = self.bbox.max_x,
      max_y = self.bbox.max_y
    },
    cells = {},
    entities = {},
    objects = {}
  }

  return data
end

function Map:export_cell_data()
  local data = {}
  for _, c in pairs(self.cells) do
    if c ~= nil then
      table.insert(data, c:export_data())
    end
  end

  return data
end

function Map:export_entity_data()
  local data = {}
  for _, e in pairs(self.entities) do
    table.insert(data, e:export_data())
  end

  return data
end

function Map:export_object_data()
  local data = {}
  for _, o in pairs(self.objects) do
    table.insert(data, o:export_data())
  end

  return data
end

function Map:import_data(data)
  self.file_path = data.file_path
  self.width = data.width
  self.height = data.height
  self.size = data.size
  self.bbox = BBox:new{
    min_x = data.bbox.min_x,
    min_y = data.bbox.min_y,
    max_x = data.bbox.max_x,
    max_y = data.bbox.max_y
  }

  self:init_phys()

  self.cells = {}
  for i = 1,#data.cells do
    local d = data.cells[i]
    local c = Cell:new{
      server_ref = self.server_ref,
      map_ref = self,
      x = d.x,
      y = d.y,
      z = d.z,
      bbox = nil,
      flag = d.flag,
      tiles = {}
    }
    for j= 1,#d.tiles do
      local dt = d.tiles[j]
      c.tiles[dt.layer] = Tile:new{
        tileset_index = dt.tileset_index,
        index = dt.index,
        layer = dt.layer
      }
    end
    self.cells[c.x + c.y * self.width] = c
  end

  self.entities = {}
  for i = 1,#data.entities do
    local d = data.entities[i]
    local e = Entity:new{
      server_ref = self.server_ref,
      map_ref = self,
      entity_data_index = d.entity_data_index,
      id = self.server_ref.entities_n,
      pos = Vec2:new{x = d.x, y = d.y}
    }
    self.server_ref.entities_n = self.server_ref.entities_n + 1
    self.entities[e.id] = e
  end

  self.objects = {}
  for i = 1,#data.objects do
    local d = data.objects[i]
    local o = Object:new{
      map_ref = self,
      object_data_index = d.object_data_index,
      id = self.server_ref.objects_n,
      pos = Vec2:new{x = d.x, y = d.y},
      custom_data = d.custom_data
    }
    self.server_ref.objects_n = self.server_ref.objects_n + 1
    self.objects[o.id] = o
  end
end

function Map:load()
  local file_ptr = assert(io.open(self.file_path, "rb"))

  -- read data
  local file_dta = file_ptr:read("*all")

  io.close(file_ptr)

  -- import data
  self:import_data(json.decode(file_dta))

  print(string.format("loaded map ID %d into memory from path %s", self.id, self.file_path))
end

function Map:update(dt)
  if self.phys ~= nil then
    self.phys.world:update(dt)
  end

  for _, e in pairs(self.entities) do
    e:update(dt)
  end

  for _, o in pairs(self.objects) do
    o:update(dt)
  end
end

function Map:get_cell_by_xy(x, y)
  if x < 0 or x >= self.width or y < 0 or y >= self.height then return nil end

  x = math.floor(x)
  y = math.floor(y)

  local index = x + y * self.width
  return self.cells[index]
end

function Map:get_entity(id)
  if id == nil then return nil end

  return self.entities[id]
end

function Map:get_entity_by_xy(x, y)
  if x < 0 or x >= self.width or y < 0 or y >= self.height then return nil end

  local entities = {}
  for _, e in pairs(self.entities) do
    if e.bbox:intersect_point(x, y) ~= nil then
      table.insert(entities, e)
    end
  end

  if #entities > 0 then return entities else return nil end
end

function Map:add_entity(x, y, entity_data_index, connection_id)
  if x < 0 or x >= self.width or y < 0 or y >= self.height then return nil end

  local e = Entity:new{
    server_ref = self.server_ref,
    map_ref = self,
    connection_id = connection_id,
    entity_data_index = entity_data_index,
    id = self.server_ref.entities_n,
    pos = Vec2:new{x = x, y = y}
  }
  self.server_ref.entities_n = self.server_ref.entities_n + 1
  self.entities[e.id] = e

  return e.id
end

function Map:del_entity(id)
  if id == nil then return end

  self.entities[id]:destroy()
  self.entities[id] = nil

  self.server_ref:add_packet_out(Packet:new{
    id = PROTOCOL_PACKETS.ENTITY_DELETE_RSP,
    map_id = self.id,
    data = {
      id = id
    }
  })
end

function Map:get_object_by_xy(x, y)
  if x < 0 or x >= self.width or y < 0 or y >= self.height then return nil end

  local objects = {}
  for _, o in pairs(self.objects) do
    if o.bbox:intersect_point(x, y) ~= nil then
      table.insert(objects, o)
    end
  end

  if #objects > 0 then return objects else return nil end
end
