require "common.base"
require "common.object_data"
require "common.vec2"
require "common.bbox"

Object = setmetatable({
  class_name = "Object",
  server_ref = nil,
  map_ref = nil,
  object_data_index = nil,
  data = nil,
  id = nil,
  pos = nil,
  bbox = nil,
  custom_data = nil
}, {__index = Base})

function Object:new(instance)
  instance = instance or {}
  setmetatable(instance, self)
  self.__index = self
  return instance:init()
end

function Object:init()
  self.object_data_index = ObjUtil.set_if_nil(self.object_data_index, 1)
  self.data = ObjectData.load(self.object_data_index)
  self.pos = ObjUtil.set_if_nil(self.pos, Vec2:new{})
  self.bbox = ObjUtil.set_if_nil(self.bbox, BBox:new{})
  self:calculate_bbox()

  print(string.format("loaded object ID %d into map ID %d", self.id, self.map_ref.id))

  return self
end

function Object:export_data()
  return {
    object_data_index = self.object_data_index,
    id = self.id,
    x = self.pos.x,
    y = self.pos.y,
    custom_data = self.custom_data
  }
end

function Object:update(dt)
end

function Object:calculate_bbox()
  self.bbox.min_x = self.pos.x - self.data.size
  self.bbox.min_y = self.pos.y - self.data.size
  self.bbox.max_x = self.pos.x + self.data.size
  self.bbox.max_y = self.pos.y + self.data.size
  self.bbox:getPos()
  self.bbox:getDim()
end
