require "common.base"
require "common.entity"
require "common.entity_data"
require "common.math_util"
require "common.vec2"
require "common.bbox"
require "server.entity_ai"
local astar = require "3rdparty.astar"

Entity = setmetatable({
  class_name = "Entity",
  server_ref = nil,
  map_ref = nil,
  connection_id = nil,
  entity_data_index = nil,
  data = nil,
  id = nil,
  pos = nil,
  bbox = nil,
  phys = nil,
  target = nil,
  state = nil,
  ai = nil
}, {__index = Base})

function Entity:new(instance)
  instance = instance or {}
  setmetatable(instance, self)
  self.__index = self
  return instance:init()
end

function Entity:init()
  self.entity_data_index = ObjUtil.set_if_nil(self.entity_data_index, 1)
  self.data = EntityData.load(self.entity_data_index)
  self.pos = ObjUtil.set_if_nil(self.pos, Vec2:new{})
  self.bbox = ObjUtil.set_if_nil(self.bbox, BBox:new{})
  self:calculate_bbox()
  self.phys = nil
  self:init_phys()
  self.target = {
    pos = nil,
    path = nil,
    state = nil,
    entity_id = nil,
    object_id = nil
  }
  self.state = ObjUtil.set_if_nil(self.state, ENTITY_STATES.idle)
  self.ai = {
    script = EntityAi[self.data.ai.script_name],
    timer = 0.0
  }

  print(string.format("loaded entity ID %d into map ID %d", self.id, self.map_ref.id))

  return self
end

function Entity:init_phys()
  if self.phys ~= nil then
    self.phys.body:destroy()
    self.phys = nil
  end

  self.phys = {
    body = love.physics.newBody(self.map_ref.phys.world, self.pos.x, self.pos.y, "dynamic"),
    shape = love.physics.newCircleShape(self.data.size),
    fixture = nil
  }
  self.phys.fixture = love.physics.newFixture(self.phys.body, self.phys.shape, 1.0)
  self.phys.body:setLinearDamping(16.0)
end

function Entity:destroy()
  if self.phys ~= nil then
    self.phys.body:destroy()
    self.phys = nil
  end
end

function Entity:export_data()
  local data = {
    entity_data_index = self.entity_data_index,
    id = self.id,
    x = self.pos.x,
    y = self.pos.y,
    s = self.state
  }
  if self.target.pos ~= nil and self.target.state ~= nil then
    data.tx = self.target.pos.x
    data.ty = self.target.pos.y
    data.ts = self.target.state
  end

  return data
end

function Entity:update(dt)
  -- update pos and bbox
  self.pos.x, self.pos.y = self.phys.body:getX(), self.phys.body:getY()
  self:calculate_bbox()

  -- run ai script if exists and entity is npc
  if self.connection_id == nil and self.ai.script ~= nil and self.ai.timer <= 0.0 then
    -- run only at certain tickrate
    self.ai.script.execute(self, dt)
    self.ai.timer = 1.0
  else
    self.ai.timer = self.ai.timer - dt
  end

  -- act according to current state
  if self.state == ENTITY_STATES.idle then
    -- pick a new target and state if exists
    if self.target.pos ~= nil and self.target.state ~= nil then
      self.state = self.target.state
    -- pick a next target from path if exists
    elseif self.target.path ~= nil then
      local target_next = table.remove(self.target.path, 1)
      if target_next ~= nil then
        self:set_target(target_next.pos.x, target_next.pos.y, target_next.state)
      else
        self.target.path = nil
      end
    end
  elseif self.state == ENTITY_STATES.move then
    -- calc delta vector
    local delta = self.target.pos - self.pos
    local delta_norm = delta:normalize()
    -- move towards delta or go back to idle
    if delta:length() > 0.1 then
      local delta_force = delta_norm:mul(self.data.speed)
      self.phys.body:applyForce(delta_force.x, delta_force.y)
    else
      self.target.pos = nil
      self.target.state = nil
      self.state = ENTITY_STATES.idle
    end
  end
end

function Entity:calculate_bbox()
  self.bbox.min_x = self.pos.x - self.data.size
  self.bbox.min_y = self.pos.y - self.data.size
  self.bbox.max_x = self.pos.x + self.data.size
  self.bbox.max_y = self.pos.y + self.data.size
  self.bbox:getPos()
  self.bbox:getDim()
end

function Entity:set_target(x, y, state)
  if state == ENTITY_STATES.move then
    self:set_target_move(x, y)
  elseif state == ENTITY_STATES.melee then
    self:set_target_melee(x, y)
  elseif state == ENTITY_STATES.magic then
    self:set_target_magic(x, y)
  else
    print(string.format("error setting entity target, invalid target state: %d", state))
  end
end

function Entity:set_target_move(x, y)
  self.target.pos = Vec2:new{x = x, y = y}
  self.target.state = ENTITY_STATES.move

  self.server_ref:add_packet_out(Packet:new{
    id = PROTOCOL_PACKETS.ENTITY_MOVE_RSP,
    map_id = self.map_ref.id,
    data = {
      id = self.id,
      x = self.target.pos.x,
      y = self.target.pos.y
    }
  })
end

function Entity:set_target_melee(x, y)
  self.target.pos = Vec2:new{x = x, y = y}
  self.target.state = ENTITY_STATES.melee

  self.server_ref:add_packet_out(Packet:new{
    id = PROTOCOL_PACKETS.ENTITY_MELEE_RSP,
    map_id = self.map_ref.id,
    data = {
      id = self.id,
      x = self.target.pos.x,
      y = self.target.pos.y
    }
  })
end

function Entity:set_target_magic(x, y)
  self.target.pos = Vec2:new{x = x, y = y}
  self.target.state = ENTITY_STATES.magic

  self.server_ref:add_packet_out(Packet:new{
    id = PROTOCOL_PACKETS.ENTITY_MAGIC_RSP,
    map_id = self.map_ref.id,
    data = {
      id = self.id,
      x = self.target.pos.x,
      y = self.target.pos.y
    }
  })
end

function Entity:set_target_path(x, y)
  -- get start & end coordinates
  local start_x = math.floor(self.pos.x) + 0.5
  local start_y = math.floor(self.pos.y) + 0.5
  local end_x = math.floor(x) + 0.5
  local end_y = math.floor(y) + 0.5

  -- get all walkable nodes
  local nodes = {}
  local node_start = nil
  local node_end = nil
  for k, v in pairs(self.map_ref.cells) do
    -- only allow walkable cells as nodes
    if v ~= nil and v.flag ~= CELL_FLAGS.block then
      local node_x = v.bbox.pos_x
      local node_y = v.bbox.pos_y
      local node_is_start = false
      local node_is_end = false
      local node_is_blocked = false

      -- check if this is a start or end node
      if node_x == start_x and node_y == start_y then
        node_is_start = true
      elseif node_x == end_x and node_y == end_y then
        node_is_end = true
      end

      -- check if any entities block the node
      local node_entities = self.map_ref:get_entity_by_xy(node_x, node_y)
      if node_entities ~= nil then
        for i, node_entity in ipairs(node_entities) do
          if node_entity.id ~= self.id and node_entity.state ~= ENTITY_STATES.dead then
            node_is_blocked = true
          end
        end
      end

      -- insert the node to our array if it's not blocked
      if node_is_blocked == false then
        local node = {
          x = node_x,
          y = node_y
        }
        table.insert(nodes, node)

        -- mark as start or end node
        if node_is_start then
          node_start = node
        elseif node_is_end then
          node_end = node
        end
      end
    end
  end

  -- execute a* algorithm if nodes found
  if nodes ~= nil and node_start ~= nil and node_end ~= nil then
    -- function that describes a valid neighbor node
    local valid_node_func = function(node, node_neighbor)
      local MAX_DIST = 1.0

      if astar.distance(node.x, node.y, node_neighbor.x, node_neighbor.y) > MAX_DIST then
        return false
      end

      return true
    end

    -- if valid path found, construct a target_path array to traverse it
    local path = astar.path(node_start, node_end, nodes, true, valid_node_func)
    if path ~= nil then
      self.target.path = {}
      for i, v in ipairs(path) do
        table.insert(self.target.path, {
          pos = Vec2:new{x = v.x, y = v.y},
          state = ENTITY_STATES.move
        })
      end
    end
  end
end
