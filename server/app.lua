require "common.base"
require "common.entity_data"
require "common.object_data"
require "server.server"

App = {
  server = nil
}

function App.load()
  -- load data
  EntityData.load_index()
  ObjectData.load_index()

  -- load server
  App.server = Server:new{}
end

function love.update(dt)
  App.server:update(dt)
end
