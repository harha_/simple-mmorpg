require "common.base"
require "common.protocol"
require "common.math_util"
require "common.message"
require "server.packet"
require "server.connection"
require "server.map"
local socketlib = require "socket"
local json = require "3rdparty.json"

Server = setmetatable({
  class_name = "Server",
  tickrate = nil,
  socket = nil,
  connections = nil,
  connections_n = nil,
  entities_n = nil,
  objects_n = nil,
  ts_packet_out = nil,
  packets_out = nil,
  maps = nil,
}, {__index = Base})

function Server:new(instance)
  instance = instance or {}
  setmetatable(instance, self)
  self.__index = self
  return instance:init()
end

function Server:init()
  self.tickrate = 1/15
  self.socket = socketlib.udp()
  self.socket:settimeout(0)
  self.socket:setsockname("*", 27050)
  self.connections = {}
  self.connections_n = 0
  self.entities_n = 0
  self.objects_n = 0
  self.ts_packet_out = nil
  self.packets_out = {}
  self.maps = {
    [1] = Map:new{server_ref = self, id = 1, file_path = "res/maps/test_big.json"}
  }

  return self
end

function Server:update(dt)
  -- process all packets in
  self:process_packets_in()

  -- process connections packet in
  for _, connection in pairs(self.connections) do
    connection:process_packets_in()
  end

  -- update maps
  for _, map in ipairs(self.maps) do
    map:update(dt)
  end

  -- update connections
  for _, connection in pairs(self.connections) do
    connection:update(dt)

    -- cleanup dead connections
    if connection.state.connected == false then
      self:del_connection(connection.id)
    end
  end

  -- process connections packet out
  for _, connection in pairs(self.connections) do
    connection:process_packets_out()
  end

  -- process all packets out
  self:process_packets_out()
end

function Server:process_packets_in()
  -- check if any data, return if not, otherwise continue
  local data_str, ip, port = self.socket:receivefrom()
  if not data_str then return end

  -- process connection data
  local pcall_status, data = pcall(json.decode, data_str)
  if not pcall_status then
    print('received invalid data: ' .. data_str)
    return
  end

  -- verify packet
  if data.id == nil then
    print('received invalid packet: ' .. data_str)
    return
  end
  local p_data = data.data

  -- forward connection packets
  if data.client_id ~= nil then
    local connection = self.connections[data.client_id]
    if connection ~= nil then
      connection:add_packet_in(data)
    end
    return
  end

  -- login request
  if data.id == PROTOCOL_PACKETS.LOGIN_REQ then
    -- create new connection
    local connection = self:add_connection(Connection:new{
      ip = ip,
      port = port,
      map_id = math.random(1, #self.maps)
    })

    -- create entity for it
    local map = self.maps[connection.map_id]
    local entity_x = math.random(0, map.width) + 0.5
    local entity_y = math.random(0, map.height) + 0.5
    connection.entity_id = map:add_entity(entity_x, entity_y, 1, connection.id)

    -- send login response
    connection:add_packet_out(Packet:new{
      id = PROTOCOL_PACKETS.LOGIN_RSP,
      client_id = connection.id,
      data = {
        token = connection.token,
        map_id = connection.map_id,
        entity_id = connection.entity_id
      }
    })

    -- send welcome message
    connection:add_packet_out(Packet:new{
      id = PROTOCOL_PACKETS.MESSAGE_RSP,
      client_id = connection.id,
      data = {
        source = MESSAGE_SOURCES.server,
        message = "Welcome to MMORPG!"
      }
    })
  end
end

function Server:add_connection(connection)
  if connection == nil or connection.ip == nil or connection.port == nil then return nil end

  connection.server_ref = self
  connection.tickrate = self.tickrate
  connection.id = self.connections_n
  connection.token = tostring(math.random(1000000000))

  self.connections[connection.id] = connection
  self.connections_n = self.connections_n + 1

  print(string.format("add connection - ip: %s, port: %d, ID: %d", connection.ip, connection.port, connection.id))

  return connection
end

function Server:del_connection(id)
  local connection = self.connections[id]

  print(string.format("del connection - ip: %s, port: %d, ID: %d", connection.ip, connection.port, connection.id))

  -- cleanup map data
  if connection.map_id ~= nil then
    local map = self.maps[connection.map_id]

    map:del_entity(connection.entity_id)
  end

  -- delete
  self.connections[id] = nil
end

function Server:add_packet_out(packet)
  if packet == nil or packet.id == nil then return end

  table.insert(self.packets_out, packet)
end

function Server:process_packets_out()
  if self.connections_n == 0 then
    self.ts_packet_out = nil
    self.packets_out = {}
    return
  end

  -- get current timestamp
  local ts = socketlib.gettime()

  -- fixed tickrate
  if self.ts_packet_out ~= nil then
    local ts_diff = ts - self.ts_packet_out
    if ts_diff < self.tickrate then
      return
    end
  end

  for _, packet in ipairs(self.packets_out) do
    local packet_data = json.encode(packet)
    for _, connection in pairs(self.connections) do
      if packet.map_id == nil or packet.map_id == connection.map_id then
        self.socket:sendto(packet_data, connection.ip, connection.port)
      end
    end
  end

  self.ts_packet_out = socketlib.gettime()
  self.packets_out = {}
end
