require "common.base"
require "common.tile"

Tile = setmetatable({
  class_name = "Tile",
  tileset_index = nil,
  index = nil,
  layer = nil
}, {__index = Base})

function Tile:new(instance)
  instance = instance or {}
  setmetatable(instance, self)
  self.__index = self
  return instance:init()
end

function Tile:init()
  self.tileset_index = ObjUtil.set_if_nil(self.tileset_index, nil)
  self.index = ObjUtil.set_if_nil(self.index, nil)
  self.layer = ObjUtil.set_if_nil(self.layer, TILE_LAYERS.floor)

  return self
end

function Tile:export_data()
  return {
    tileset_index = self.tileset_index,
    index = self.index,
    layer = self.layer
  }
end
