require "common.vec2"
require "common.vec3"
require "common.bbox"

IsoMath = {}

IsoMath.scale = 1.0
IsoMath.tile_width = 128
IsoMath.tile_width_h = IsoMath.tile_width / 2
IsoMath.tile_height = 64
IsoMath.tile_height_h = IsoMath.tile_height / 2

function IsoMath.world_xyz_to_screen_xy(x, y, z, off_x, off_y, off_z)
  -- add offset (world coords)
  x = x + off_x
  y = y + off_y
  z = z + off_z

  -- return projected coordinates
  return  ((x - y) * IsoMath.tile_width_h) + love.graphics.getWidth() / IsoMath.scale * 0.5,
          ((x + y) * IsoMath.tile_height_h) + love.graphics.getHeight() / IsoMath.scale * 0.5 - z * IsoMath.tile_height_h
end

function IsoMath.world_bbox_to_screen_bbox(bbox, z, scale, off_x, off_y, off_z)
  local screen_min_x, screen_min_y = IsoMath.world_xyz_to_screen_xy(bbox.min_x, bbox.min_y, z, off_x, off_y, off_z)
  local screen_max_x, screen_max_y = IsoMath.world_xyz_to_screen_xy(bbox.max_x, bbox.max_y, z, off_x, off_y, off_z)
  local screen_bbox = BBox:new{
    min_x = screen_min_x - (screen_max_y - screen_min_y) * scale,
    min_y = screen_min_y,
    max_x = screen_max_x + (screen_max_y - screen_min_y) * scale,
    max_y = screen_max_y
  }

  return screen_bbox
end

function IsoMath.screen_xy_to_world_xyz(x, y, off_x, off_y, off_z)
  -- add offset (screen coords)
  x = (x - love.graphics.getWidth() * 0.5) / IsoMath.scale
  y = (y - love.graphics.getHeight() * 0.5) / IsoMath.scale

  -- return unprojected coordinates
  return  ((x / IsoMath.tile_width_h + y / IsoMath.tile_height_h) / 2) - off_x,
          ((y / IsoMath.tile_height_h - (x / IsoMath.tile_width_h)) / 2) - off_y,
          0.0 - off_z
end

function IsoMath.cull_by_distance(world_pos, world_offset_pos, render_distance)
  if world_pos == nil or world_offset_pos == nil then return true end
  
  local world_distance = (world_offset_pos - world_pos):length()
  return world_distance > render_distance, world_distance
end
