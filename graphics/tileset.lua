require "common.base"
require "common.tile"
require "graphics.iso_render"
local json = require "3rdparty.json"

TILESET_INDEX = {}
TILESET_LOADED = {}

Tileset = setmetatable({
  class_name = "Tileset",
  index = nil,
  identifier = nil,
  file_path = nil,      -- tileset file path
  image = nil,          -- loaded tileset image data
  width = nil,          -- tileset width in tiles
  height = nil,         -- tileset height in tiles
  x_skip = 0,           -- tileset x skip in pixels
  y_skip = 0,           -- tileset y skip in pixels
  tile_w = nil,         -- tile width in pixels
  tile_h = nil,         -- tile height in pixels
  tile_c_x = nil,       -- tile x center in pixels
  tile_c_y = nil,       -- tile y center in pixels
  quads = nil           -- split individual tile quad data
}, {__index = Base})

function Tileset.load_index()
  -- read data
  local file_ptr = assert(io.open("res/tileset_index.json", "rb"))
  local data = json.decode(file_ptr:read("*all"))
  io.close(file_ptr)

  -- store into memory
  for k, v in ipairs(data) do
    TILESET_INDEX[v.index] = v
  end

  print("loaded tileset index into memory...")
end

function Tileset.load(index)
  local ref = TILESET_LOADED[index]

  -- load existing
  if ref ~= nil then
    return ref
  end

  -- load new
  TILESET_LOADED[index] = Tileset:new{index = index}

  return TILESET_LOADED[index]
end

function Tileset:new(instance)
  instance = instance or {}
  setmetatable(instance, self)
  self.__index = self
  return instance:init()
end

function Tileset:init()
  local tileset = TILESET_INDEX[self.index]

  self.identifier = tileset.identifier
  self.file_path = tileset.file_path
  self.width = tileset.width
  self.height = tileset.height
  self.x_skip = tileset.x_skip
  self.y_skip = tileset.y_skip
  self.tile_w = tileset.tile_w
  self.tile_h = tileset.tile_h
  self.tile_c_x = tileset.tile_c_x
  self.tile_c_y = tileset.tile_c_y
  self.image = love.graphics.newImage(self.file_path)
  self.image:setFilter("nearest", "nearest")
  self.quads = {}
  for x = 0, self.width - 1 do
    for y = 0, self.height - 1 do
      self.quads[x + y * self.width] = love.graphics.newQuad(
        x * self.tile_w + (x * self.x_skip),
        y * self.tile_h + (y * self.y_skip),
        self.tile_w,
        self.tile_h,
        self.image:getDimensions()
      )
    end
  end

  print("loaded new tileset into memory: " .. self.file_path)

  return self
end

function Tileset:draw_tile(index, rgba, screen_x, screen_y)
  if index < 0 or index >= self.width * self.height then
    return
  end

  love.graphics.setColor(rgba.r, rgba.g, rgba.b, rgba.a)
  love.graphics.draw(self.image, self.quads[index], screen_x, screen_y)
  love.graphics.setColor(1.0, 1.0, 1.0, 1.0)
end

function Tileset:draw_tile_iso(index, rgba, layer, w_pos, w_ofs)
  if index < 0 or index >= self.width * self.height then
    return
  end

  -- init world xyz-offset for z-sorting
  local z_ofs = Vec3:new{x = 0.5, y = 0.5, z = 0}

  -- set z-offset by layer
  if layer == TILE_LAYERS.decal then
    z_ofs.z = 0.1
  elseif layer == TILE_LAYERS.object then
    z_ofs.z = 0.2
  elseif layer == TILE_LAYERS.gui then
    z_ofs.z = 1000.0
  end

  IsoRender.add_render_cmd(IsoQuad:new{
    image = self.image,
    quad = self.quads[index],
    rgba = rgba,
    w_pos = w_pos,
    w_ofs = w_ofs,
    z_ofs = z_ofs,
    s_ofs = Vec2:new{x = self.tile_c_x, y = self.tile_c_y}
  })
end
