require "common.base"

IsoQuad = setmetatable({
  class_name = "IsoQuad",
  image = nil,          -- referenced isoquad image data
  quad = nil,           -- referenced split individual quad data
  rgba = nil,           -- isoquad rgba-color
  w_pos = nil,          -- isoquad world xyz-coordinates in world units
  w_ofs = nil,          -- isoquad world xyz-offset in world units
  z_ofs = nil,          -- isoquad world xyz-offset in world units for z-sorting
  s_pos = nil,          -- isoquad screen xy-coordinates in pixels
  s_ofs = nil           -- isoquad screen xy-offset in pixels
}, {__index = Base})

function IsoQuad:new(instance)
  instance = instance or {}
  setmetatable(instance, self)
  self.__index = self
  return instance:init()
end

function IsoQuad:init()
  return self
end
