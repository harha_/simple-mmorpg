require "common.base"
require "common.obj_util"
local json = require "3rdparty.json"

ANIMATIONSHEET_INDEX = {}
ANIMATIONSHEET_LOADED = {}

Animationsheet = setmetatable({
  class_name = "Animationsheet",
  index = nil,
  identifier = nil,
  file_path = nil,
  width = nil,
  height = nil,
  rate = nil,
  animations = nil
}, {__index = Base})

function Animationsheet.load_index()
  -- read data
  local file_ptr = assert(io.open("res/animationsheet_index.json", "rb"))
  local data = json.decode(file_ptr:read("*all"))
  io.close(file_ptr)

  -- store into memory
  for k, v in ipairs(data) do
    ANIMATIONSHEET_INDEX[v.index] = v
  end

  print("loaded animationsheet index into memory...")
end

function Animationsheet.load(index)
  local ref = ANIMATIONSHEET_LOADED[index]

  -- load existing
  if ref ~= nil then
    return ref
  end

  -- load new
  ANIMATIONSHEET_LOADED[index] = Animationsheet:new{index = index}

  return ANIMATIONSHEET_LOADED[index]
end

function Animationsheet:new(instance)
  instance = instance or {}
  setmetatable(instance, self)
  self.__index = self
  return instance:init()
end

function Animationsheet:init()
  local animationsheet = ANIMATIONSHEET_INDEX[self.index]

  self.identifier = animationsheet.identifier
  self.file_path = animationsheet.file_path
  self.width = animationsheet.width
  self.height = animationsheet.height
  self.rate = animationsheet.rate

  -- read data
  local file_ptr = assert(io.open(self.file_path, "rb"))
  local data = json.decode(file_ptr:read("*all"))
  io.close(file_ptr)

  -- import data
  self.animations = {}
  for k, v in ipairs(data) do
    self.animations[v.index] = v
  end

  print("loaded new animationsheet data into memory: " .. self.file_path)

  return self
end
