require "common.base"
require "common.obj_util"

RGBA = setmetatable({
  class_name = "RGBA",
  r = nil,
  g = nil,
  b = nil,
  a = nil
}, {__index = Base})

function RGBA:new(instance)
  instance = instance or {}
  setmetatable(instance, self)
  self.__index = self
  return instance:init()
end

function RGBA:init()
  self.r = ObjUtil.set_if_nil(self.r, 0)
  self.g = ObjUtil.set_if_nil(self.g, 0)
  self.b = ObjUtil.set_if_nil(self.b, 0)
  self.a = ObjUtil.set_if_nil(self.a, 1)

  return self
end

function RGBA:__add(v)
  return RGBA:new{
    r = self.r + v.r,
    g = self.g + v.g,
    b = self.b + v.b,
    a = self.a + v.a
  }
end

function RGBA:__sub(v)
  return RGBA:new{
    r = self.r - v.r,
    g = self.g - v.g,
    b = self.b - v.b,
    a = self.a - v.a
  }
end
