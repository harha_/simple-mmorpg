require "common.base"
require "graphics.animationsheet"
require "graphics.iso_render"
local json = require "3rdparty.json"

SPRITESHEET_INDEX = {}
SPRITESHEET_LOADED = {}

Spritesheet = setmetatable({
  class_name = "Spritesheet",
  index = nil,
  identifier = nil,
  file_path = nil,      -- spritesheet filepath
  image = nil,          -- loaded spritesheet image data
  width = nil,          -- spritesheet width in sprites
  height = nil,         -- spritesheet height in sprites
  x_skip = 0,           -- spritesheet x skip in pixels
  y_skip = 0,           -- spritesheet y skip in pixels
  sprite_w = nil,       -- sprite width in pixels
  sprite_h = nil,       -- sprite height in pixels
  sprite_c_x = nil,     -- sprite x center in pixels
  sprite_c_y = nil,     -- sprite y center in pixels
  quads = nil,          -- split individual sprite quad data
  animationsheet = nil  -- loaded animationsheet data
}, {__index = Base})

function Spritesheet.load_index()
  -- read data
  local file_ptr = assert(io.open("res/spritesheet_index.json", "rb"))
  local data = json.decode(file_ptr:read("*all"))
  io.close(file_ptr)

  -- store into memory
  for k, v in ipairs(data) do
    SPRITESHEET_INDEX[v.index] = v
  end

  print("loaded spritesheet index into memory...")
end

function Spritesheet.load(index)
  local ref = SPRITESHEET_LOADED[index]

  -- load existing
  if ref ~= nil then
    return ref
  end

  -- load new
  SPRITESHEET_LOADED[index] = Spritesheet:new{index = index}

  return SPRITESHEET_LOADED[index]
end

function Spritesheet:new(instance)
  instance = instance or {}
  setmetatable(instance, self)
  self.__index = self
  return instance:init()
end

function Spritesheet:init()
  local spritesheet = SPRITESHEET_INDEX[self.index]

  self.identifier = spritesheet.identifier
  self.file_path = spritesheet.file_path
  self.width = spritesheet.width
  self.height = spritesheet.height
  self.x_skip = spritesheet.x_skip
  self.y_skip = spritesheet.y_skip
  self.sprite_w = spritesheet.sprite_w
  self.sprite_h = spritesheet.sprite_h
  self.sprite_c_x = spritesheet.sprite_c_x
  self.sprite_c_y = spritesheet.sprite_c_y
  self.image = love.graphics.newImage(self.file_path)
  self.image:setFilter("nearest", "nearest")
  self.quads = {}
  if spritesheet.animationsheet_index ~= nil then
    self.animationsheet = Animationsheet.load(spritesheet.animationsheet_index)
  end
  for x = 0, self.width - 1 do
    for y = 0, self.height - 1 do
      self.quads[x + y * self.width] = love.graphics.newQuad(
        x * self.sprite_w + (x * self.x_skip),
        y * self.sprite_h + (y * self.y_skip),
        self.sprite_w,
        self.sprite_h,
        self.image:getDimensions()
      )
    end
  end

  print("loaded new spritesheet into memory: " .. self.file_path)

  return self
end

function Spritesheet:draw_sprite(index, rgba, screen_x, screen_y)
  if index < 0 or index >= self.width * self.height then
    return
  end

  love.graphics.setColor(rgba.r, rgba.g, rgba.b, rgba.a)
  love.graphics.draw(self.image, self.quads[index], screen_x, screen_y)
  love.graphics.setColor(1.0, 1.0, 1.0, 1.0)
end

function Spritesheet:draw_sprite_iso(index, rgba, layer, w_pos, w_ofs)
  if index < 0 or index >= self.width * self.height then
    return
  end

  -- init world xy-offset for z-sorting
  local z_ofs = Vec3:new{x = 0.5, y = 0.5, z = 0}

  -- set z-offset by layer
  if layer == SPRITE_LAYERS.gui then
    z_ofs.z = 1000.0
  end

  IsoRender.add_render_cmd(IsoQuad:new{
    image = self.image,
    quad = self.quads[index],
    rgba = rgba,
    w_pos = w_pos,
    w_ofs = w_ofs,
    z_ofs = z_ofs,
    s_ofs = Vec2:new{x = self.sprite_c_x, y = self.sprite_c_y}
  })
end
