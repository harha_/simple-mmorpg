require "common.base"
require "graphics.iso_math"
require "graphics.iso_quad"

IsoRender = {
  render_cmds = {}
}

function IsoRender.add_render_cmd(iq)
  -- project from world to screen
  local screen_x, screen_y = IsoMath.world_xyz_to_screen_xy(iq.w_pos.x, iq.w_pos.y, iq.w_pos.z, iq.w_ofs.x, iq.w_ofs.y, iq.w_ofs.z)

  -- modify the render cmd accordingly
  iq.s_pos = Vec2:new{
    x = screen_x - iq.s_ofs.x,
    y = screen_y - iq.s_ofs.y
  }

  -- insert the render cmd to our buffer
  table.insert(IsoRender.render_cmds, iq)
end

function IsoRender.execute_render_cmds()
  -- init
  love.graphics.push()
  love.graphics.scale(IsoMath.scale, IsoMath.scale)
  love.graphics.setBackgroundColor(0.4, 0.6, 0.8, 1)

  -- sort render cmds
  table.sort(IsoRender.render_cmds, function(a, b)
    return
      ((a.w_pos.x + a.z_ofs.x) + (a.w_pos.y + a.z_ofs.y) + (a.w_pos.z + a.z_ofs.z))
        <
      ((b.w_pos.x + b.z_ofs.x) + (b.w_pos.y + b.z_ofs.y) + (b.w_pos.z + b.z_ofs.z))
  end)

  -- render layer by layer
  for _, v in ipairs(IsoRender.render_cmds) do
    -- set alpha
    love.graphics.setColor(v.rgba.r, v.rgba.g, v.rgba.b, v.rgba.a)
    -- draw
    love.graphics.draw(v.image, v.quad, v.s_pos.x, v.s_pos.y)
    -- reset alpha
    love.graphics.setColor(1.0, 1.0, 1.0, 1.0)
  end

  -- cleanup
  IsoRender.render_cmds = {}
  love.graphics.pop()
end
