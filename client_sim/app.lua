require "common.base"
require "common.entity_data"
require "common.object_data"
require "client_sim.simulator"

App = {
  simulator = nil
}

function App.load()
  -- load simulator
  App.simulator = Simulator:new{client_n = 128}
end

function love.update(dt)
  App.simulator:update(dt)
end
