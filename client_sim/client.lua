require "common.base"
require "common.protocol"
require "common.math_util"
require "common.vec2"
require "common.vec3"
require "common.bbox"
require "client_sim.packet"
require "client_sim.connection"
require "client_sim.chat_messages"
local socketlib = require "socket"
local json = require "3rdparty.json"

Client = setmetatable({
  class_name = "Client",
  tickrate = nil,
  socket = nil,
  connection = nil,
  ts_packet_out = nil,
  packets_out = nil,
  map = nil,
  entity = nil,
  state = nil
}, {__index = Base})

function Client:new(instance)
  instance = instance or {}
  setmetatable(instance, self)
  self.__index = self
  return instance:init()
end

function Client:init()
  self.tickrate = 1/30
  self.socket = socketlib.udp()
  self.socket:settimeout(0)
  self.socket:setpeername("127.0.0.1", 27050)
  self.connection = Connection:new{}
  self.ts_packet_out = nil
  self.packets_out = {}
  self.map = {
    min_x = 0,
    min_y = 0,
    max_x = 0,
    max_y = 0
  }
  self.entity = {
    x = 0,
    y = 0
  }
  self.state = ObjUtil.set_if_nil(self.state, {
    latency_in_ms = 0.0,
    move_timer = 0.0,
    chat_timer = 0.0
  })

  -- send login request
  self:add_packet_out(Packet:new{
    id = PROTOCOL_PACKETS.LOGIN_REQ
  })

  return self
end

function Client:update(dt)
  -- process incoming packets
  self:process_packets_in()

  -- roam randomly in the map
  if self.state.move_timer <= 0.0 then
    local roam_chance = MathUtil.randomf(0.0, 1.0)
    if roam_chance > 0.9 then
      local roam_r = 3.0
      local roam_x = MathUtil.randomf(self.entity.x - roam_r, self.entity.x + roam_r)
      local roam_y = MathUtil.randomf(self.entity.y - roam_r, self.entity.y + roam_r)
      self:add_packet_out(Packet:new{
        id = PROTOCOL_PACKETS.ENTITY_MOVE_REQ,
        client_id = self.connection.id,
        token = self.connection.token,
        data = {
          id = self.connection.entity_id,
          x = roam_x,
          y = roam_y
        }
      })
      self.state.move_timer = MathUtil.randomf(1, 5)
    end
  end
  self.state.move_timer = self.state.move_timer - dt

  -- send random chat messages
  if self.state.chat_timer <= 0.0 then
    local chat_chance = MathUtil.randomf(0.0, 1.0)
    if chat_chance > 0.9 then
      self:add_packet_out(Packet:new{
        id = PROTOCOL_PACKETS.MESSAGE_REQ,
        client_id = self.connection.id,
        token = self.connection.token,
        data = {
          message = ChatMessages[math.random(#ChatMessages)]
        }
      })
      self.state.chat_timer = MathUtil.randomf(1, 30)
    end
  end
  self.state.chat_timer = self.state.chat_timer - dt

  -- process outgoing packets
  self:process_packets_out()
end

function Client:process_packets_in()
  -- check if any data, return if not, otherwise continue
  local data_str, ip, port = self.socket:receive()
  if not data_str then return end

  -- process connection data
  local pcall_status, data = pcall(json.decode, data_str)
  if not pcall_status then
    print('received invalid data: ' .. data_str)
    return
  end

  -- verify packet
  if data.id == nil then
    print('received invalid packet: ' .. data_str)
    return
  end
  local p_data = data.data

  -- pong request
  if data.id == PROTOCOL_PACKETS.PING then
    self:add_packet_out(Packet:new{
      id = PROTOCOL_PACKETS.PONG,
      client_id = self.connection.id,
      token = self.connection.token
    })
    self.state.latency_in_ms = p_data.latency_in_ms

  -- login response
  elseif data.id == PROTOCOL_PACKETS.LOGIN_RSP then
    print(string.format("LOGIN_RSP, client_id: %d", data.client_id))

    self.connection = Connection:new{
      id = data.client_id,
      token = p_data.token,
      map_id = p_data.map_id,
      entity_id = p_data.entity_id
    }

  -- message response
  elseif data.id == PROTOCOL_PACKETS.MESSAGE_RSP then
    print(string.format("MESSAGE_RSP, client_id: %d", self.connection.id))

  -- map sync response
  elseif data.id == PROTOCOL_PACKETS.MAP_SYNC_RSP then
    print(string.format("MAP_SYNC_RSP, client_id: %d", self.connection.id))

    self.map.min_x = p_data.bbox.min_x
    self.map.min_y = p_data.bbox.min_y
    self.map.max_x = p_data.bbox.max_x
    self.map.max_y = p_data.bbox.max_y

  -- cell sync response
  elseif data.id == PROTOCOL_PACKETS.CELL_SYNC_RSP then
    print(string.format("CELL_SYNC_RSP, client_id: %d", self.connection.id))

  -- entity sync response
  elseif data.id == PROTOCOL_PACKETS.ENTITY_SYNC_RSP then
    print(string.format("ENTITY_SYNC_RSP, client_id: %d", self.connection.id))

    for i = 1,#p_data do
      local d = p_data[i]
      if d.id == self.connection.entity_id then
        self.entity.x = d.x
        self.entity.y = d.y
      end
    end

  -- entity delete response
  elseif data.id == PROTOCOL_PACKETS.ENTITY_DELETE_RSP then
    print(string.format("ENTITY_DELETE_RSP, client_id: %d", self.connection.id))

  -- entity move response
  elseif data.id == PROTOCOL_PACKETS.ENTITY_MOVE_RSP then
    print(string.format("ENTITY_MOVE_RSP, client_id: %d", self.connection.id))

  -- object sync response
  elseif data.id == PROTOCOL_PACKETS.OBJECT_SYNC_RSP then
    print(string.format("OBJECT_SYNC_RSP, client_id: %d", self.connection.id))
  end
end

function Client:add_packet_out(packet)
  if packet == nil or packet.id == nil then return end

  table.insert(self.packets_out, packet)
end

function Client:process_packets_out()
  -- get current timestamp
  local ts = socketlib.gettime()

  -- fixed tickrate
  if self.ts_packet_out ~= nil then
    local ts_diff = ts - self.ts_packet_out
    if ts_diff < self.tickrate then
      return
    end
  end

  for _, packet in ipairs(self.packets_out) do
    self.socket:send(json.encode(packet))
  end

  self.ts_packet_out = socketlib.gettime()
  self.packets_out = {}
end
