require "common.base"
require "common.protocol"
require "common.message"
require "common.math_util"
require "client_sim.client"
require "client_sim.packet"
require "client_sim.connection"
local socketlib = require "socket"
local json = require "3rdparty.json"

Simulator = setmetatable({
  class_name = "Simulator",
  client_n = nil,
  clients = nil
}, {__index = Base})

function Simulator:new(instance)
  instance = instance or {}
  setmetatable(instance, self)
  self.__index = self
  return instance:init()
end

function Simulator:init()
  self.client_n = ObjUtil.set_if_nil(self.client_n, 10)
  self.clients = {}

  return self
end

function Simulator:update(dt)
  -- add clients randomly
  if #self.clients < self.client_n then
    local client_chance = MathUtil.randomf(0.0, 1.0)
    if client_chance > 0.99 then
      table.insert(self.clients, Client:new{})
    end
  end

  for _, client in ipairs(self.clients) do
    client:update(dt)
  end
end
