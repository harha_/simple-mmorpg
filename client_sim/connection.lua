require "common.base"

Connection = setmetatable({
  class_name = "Connection",
  id = nil,
  token = nil,
  map_id = nil,
  entity_id = nil
}, {__index = Base})

function Connection:new(instance)
  instance = instance or {}
  setmetatable(instance, self)
  self.__index = self
  return instance:init()
end

function Connection:init()
  return self
end
