require "common.base"
require "common.vec2"
require "common.vec3"
require "common.bbox"
require "common.math_util"
require "graphics.iso_render"
require "map_editor.map"
require "map_editor.cell"
require "map_editor.tile"
require "map_editor.gui"

EDITOR_MODES = {
  cell = 1,
  tile = 2,
  entity = 3,
  object = 4
}

EDITOR_ACTIONS = {
  none = 1,
  select_cell = 2,
  insert_cell = 3,
  modify_cell = 4,
  erase_cell = 5,
  select_tile = 6,
  insert_tile = 7,
  modify_tile = 8,
  erase_tile = 9,
  select_entity = 10,
  insert_entity = 11,
  modify_entity = 12,
  erase_entity = 13,
  select_object = 14,
  insert_object = 15,
  modify_object = 16,
  erase_object = 17
}

Editor = setmetatable({
  class_name = "Editor",
  map = nil,            -- editor Map
  mouse = nil,          -- editor Mouse
  camera = nil,         -- editor Camera
  gui = nil,            -- editor GUI
  tileset = nil,        -- editor GUI tileset
  state = nil           -- editor State
}, {__index = Base})

function Editor:new(instance)
  instance = instance or {}
  setmetatable(instance, self)
  self.__index = self
  return instance:init()
end

function Editor:init()
  self.map = ObjUtil.set_if_nil(self.map, Map:new{})
  self.mouse = ObjUtil.set_if_nil(self.mouse, {
    s_pos = Vec2:new{},
    w_pos = Vec3:new{},
    w_bbox = BBox:new{}
  })
  self.camera = ObjUtil.set_if_nil(self.camera, {
    w_pos = Vec3:new{x = self.map.width / 2, y = self.map.height / 2, z = 0},
    w_ofs = Vec3:new{},
    speed = 6.0
  })
  self.tileset = ObjUtil.set_if_nil(self.tileset, Tileset.load(1))
  self.state = ObjUtil.set_if_nil(self.state, {
    mode = EDITOR_MODES.tile,
    action = EDITOR_ACTIONS.none,
    cell_flag = CELL_FLAGS.walk,
    tileset_index = 2,
    tile_index = 0,
    tile_layer = TILE_LAYERS.floor,
    tileset_ref = nil,
    entity_data_index = 1,
    entity_data_ref = nil,
    object_data_index = 1,
    object_data_ref = nil,
    spritesheet_ref = nil
  })
  self.gui = ObjUtil.set_if_nil(self.gui, GUI:new{
    editor_ref = self
  })

  return self
end

function Editor:textinput(text)

end

function Editor:keypressed(key, scancode, isrepeat)

end

function Editor:keyreleased(key, scancode, isrepeat)

end

function Editor:wheelmoved(x, y)
  local dx, dy = 0, 0
  if x > 0 then dx = 1 elseif x < 0 then dx = -1 end
  if y > 0 then dy = 1 elseif y < 0 then dy = -1 end

  -- tile mode: tile_index
  if self.state.mode == EDITOR_MODES.tile then
    self.state.tile_index = self.state.tile_index + dy
    self.state.tile_index = math.max(self.state.tile_index, 0)
  -- entity mode: entity_data_index
  elseif self.state.mode == EDITOR_MODES.entity then
    self.state.entity_data_index = self.state.entity_data_index + dy
    self.state.entity_data_index = math.max(self.state.entity_data_index, 1)
    self.state.entity_data_index = math.min(self.state.entity_data_index, #ENTITY_DATA_INDEX)
  -- object mode: object_data_index
  elseif self.state.mode == EDITOR_MODES.object then
    self.state.object_data_index = self.state.object_data_index + dy
    self.state.object_data_index = math.max(self.state.object_data_index, 1)
    self.state.object_data_index = math.min(self.state.object_data_index, #OBJECT_DATA_INDEX)
  end
end

function Editor:mousepressed(x, y, button, istouch, presses)
  if self.gui.hovered then return end

  -- cell mode
  if self.state.mode == EDITOR_MODES.cell then
    if button == 1 then
      self.state.action = EDITOR_ACTIONS.insert_cell
    elseif button == 2 then
      self.state.action = EDITOR_ACTIONS.erase_cell
    elseif button == 3 then
      self.state.action = EDITOR_ACTIONS.select_cell
    end
  -- tile mode
  elseif self.state.mode == EDITOR_MODES.tile then
    if button == 1 then
      self.state.action = EDITOR_ACTIONS.insert_tile
    elseif button == 2 then
      self.state.action = EDITOR_ACTIONS.erase_tile
    elseif button == 3 then
      self.state.action = EDITOR_ACTIONS.select_tile
    end
  -- entity mode
  elseif self.state.mode == EDITOR_MODES.entity then
    if button == 1 then
      self.state.action = EDITOR_ACTIONS.insert_entity
    elseif button == 2 then
      self.state.action = EDITOR_ACTIONS.erase_entity
    elseif button == 3 then
      self.state.action = EDITOR_ACTIONS.select_entity
    end
  -- object mode
  elseif self.state.mode == EDITOR_MODES.object then
    if button == 1 then
      self.state.action = EDITOR_ACTIONS.insert_object
    elseif button == 2 then
      self.state.action = EDITOR_ACTIONS.erase_object
    elseif button == 3 then
      self.state.action = EDITOR_ACTIONS.select_object
    end
  end
end

function Editor:mousereleased(x, y, button, istouch, presses)
  -- cell mode
  if self.state.action == EDITOR_ACTIONS.insert_cell then
    self.map:set_cell(self.mouse.w_pos:floor().x, self.mouse.w_pos:floor().y, self.mouse.w_pos:snap(0.5).z, self.state.cell_flag)
  elseif self.state.action == EDITOR_ACTIONS.erase_cell then
    self.map:erase_cell(self.mouse.w_pos:floor().x, self.mouse.w_pos:floor().y)
  elseif self.state.action == EDITOR_ACTIONS.select_cell then

  -- tile mode
  elseif self.state.action == EDITOR_ACTIONS.insert_tile then
    self.map:set_tile(self.mouse.w_pos:floor().x, self.mouse.w_pos:floor().y, self.mouse.w_pos:snap(0.5).z, self.state.tileset_index, self.state.tile_index, self.state.tile_layer)
  elseif self.state.action == EDITOR_ACTIONS.erase_tile then
    self.map:erase_tile(self.mouse.w_pos:floor().x, self.mouse.w_pos:floor().y, self.state.tile_layer)
  elseif self.state.action == EDITOR_ACTIONS.select_tile then

  -- entity mode
  elseif self.state.action == EDITOR_ACTIONS.insert_entity then
    self.map:set_entity(self.mouse.w_pos:floor().x, self.mouse.w_pos:floor().y, self.state.entity_data_index)
  elseif self.state.action == EDITOR_ACTIONS.erase_entity then
    self.map:erase_entity(self.mouse.w_pos:floor().x, self.mouse.w_pos:floor().y)
  elseif self.state.action == EDITOR_ACTIONS.select_entity then

  -- object mode
  elseif self.state.action == EDITOR_ACTIONS.insert_object then
    self.map:set_object(self.mouse.w_pos:floor().x, self.mouse.w_pos:floor().y, self.state.object_data_index, "")
  elseif self.state.action == EDITOR_ACTIONS.erase_object then
    self.map:erase_object(self.mouse.w_pos:floor().x, self.mouse.w_pos:floor().y)
  elseif self.state.action == EDITOR_ACTIONS.select_object then
    self.gui:select_object(self.map:get_object_by_xy(self.mouse.w_pos:floor().x, self.mouse.w_pos:floor().y))
  end

  -- reset action
  self.state.action = EDITOR_ACTIONS.none
end

function Editor:mousemoved(x, y, dx, dy, istouch)
  if self.gui.hovered then return end

  -- cell mode
  if self.state.action == EDITOR_ACTIONS.insert_cell then
    self.map:set_cell(self.mouse.w_pos:floor().x, self.mouse.w_pos:floor().y, self.mouse.w_pos:snap(0.5).z, self.state.cell_flag)
  elseif self.state.action == EDITOR_ACTIONS.erase_cell then
    self.map:erase_cell(self.mouse.w_pos:floor().x, self.mouse.w_pos:floor().y)
  elseif self.state.action == EDITOR_ACTIONS.select_cell then

  -- tile mode
  elseif self.state.action == EDITOR_ACTIONS.insert_tile then
    self.map:set_tile(self.mouse.w_pos:floor().x, self.mouse.w_pos:floor().y, self.mouse.w_pos:snap(0.5).z, self.state.tileset_index, self.state.tile_index, self.state.tile_layer)
  elseif self.state.action == EDITOR_ACTIONS.erase_tile then
    self.map:erase_tile(self.mouse.w_pos:floor().x, self.mouse.w_pos:floor().y, self.state.tile_layer)
  elseif self.state.action == EDITOR_ACTIONS.select_tile then

  -- entity mode
  elseif self.state.action == EDITOR_ACTIONS.insert_entity then
    self.map:set_entity(self.mouse.w_pos:floor().x, self.mouse.w_pos:floor().y, self.state.entity_data_index)
  elseif self.state.action == EDITOR_ACTIONS.erase_entity then
    self.map:erase_entity(self.mouse.w_pos:floor().x, self.mouse.w_pos:floor().y)
  elseif self.state.action == EDITOR_ACTIONS.select_entity then

  -- object mode
  elseif self.state.action == EDITOR_ACTIONS.insert_object then
    self.map:set_object(self.mouse.w_pos:floor().x, self.mouse.w_pos:floor().y, self.state.object_data_index, "")
  elseif self.state.action == EDITOR_ACTIONS.erase_object then
    self.map:erase_object(self.mouse.w_pos:floor().x, self.mouse.w_pos:floor().y)
  elseif self.state.action == EDITOR_ACTIONS.select_object then
    self.gui:select_object(self.map:get_object_by_xy(self.mouse.w_pos:floor().x, self.mouse.w_pos:floor().y))
  end
end

function Editor:update(dt)
  -- update mouse XY-coordinates
  self.mouse.s_pos.x, self.mouse.s_pos.y = love.mouse.getPosition()
  self.mouse.w_pos.x, self.mouse.w_pos.y, self.mouse.w_pos.z = IsoMath.screen_xy_to_world_xyz(self.mouse.s_pos.x, self.mouse.s_pos.y, self.camera.w_ofs.x, self.camera.w_ofs.y, self.camera.w_ofs.z)

  -- update mouse BBox-coordinates
  self.mouse.w_bbox.min_x = self.mouse.w_pos:floor().x
  self.mouse.w_bbox.min_y = self.mouse.w_pos:floor().y
  self.mouse.w_bbox.max_x = self.mouse.w_pos:floor().x + 1
  self.mouse.w_bbox.max_y = self.mouse.w_pos:floor().y + 1
  self.mouse.w_bbox:getPos()
  self.mouse.w_bbox:getDim()

  -- update refs to currently selected data
  self.state.tileset_ref = Tileset.load(self.state.tileset_index)
  self.state.entity_data_ref = EntityData.load(self.state.entity_data_index)
  self.state.object_data_ref = ObjectData.load(self.state.object_data_index)

  -- entity mode
  if self.state.mode == EDITOR_MODES.entity then
    self.state.spritesheet_ref = Spritesheet.load(self.state.entity_data_ref.spritesheet_indexes.idle)
  elseif self.state.mode == EDITOR_MODES.object then
    self.state.spritesheet_ref = Spritesheet.load(self.state.object_data_ref.spritesheet_indexes.default)
  end

  -- move camera
  if love.keyboard.isDown("up") then
    self.camera.w_pos.x = self.camera.w_pos.x - self.camera.speed * dt
    self.camera.w_pos.y = self.camera.w_pos.y - self.camera.speed * dt
  elseif love.keyboard.isDown("down") then
    self.camera.w_pos.x = self.camera.w_pos.x + self.camera.speed * dt
    self.camera.w_pos.y = self.camera.w_pos.y + self.camera.speed * dt
  end
  if love.keyboard.isDown("right") then
    self.camera.w_pos.x = self.camera.w_pos.x + self.camera.speed * 0.5 * dt
    self.camera.w_pos.y = self.camera.w_pos.y - self.camera.speed * 0.5 * dt
  elseif love.keyboard.isDown("left") then
    self.camera.w_pos.x = self.camera.w_pos.x - self.camera.speed * 0.5 * dt
    self.camera.w_pos.y = self.camera.w_pos.y + self.camera.speed * 0.5 * dt
  end
  if love.keyboard.isDown("pageup") then
    self.camera.w_pos.z = self.camera.w_pos.z + self.camera.speed * 0.5 * dt
  elseif love.keyboard.isDown("pagedown") then
    self.camera.w_pos.z = self.camera.w_pos.z - self.camera.speed * 0.5 * dt
  end
  self.camera.w_ofs.x = -self.camera.w_pos.x
  self.camera.w_ofs.y = -self.camera.w_pos.y
  self.camera.w_ofs.z = MathUtil.snap(-self.camera.w_pos.z, 0.5)

  -- update gui
  self.gui:update(dt)
end

function Editor:render()
  -- render map
  self.map:render(self.camera.w_ofs.x, self.camera.w_ofs.y, self.camera.w_ofs.z)

  -- render editor cursor
  self.tileset:draw_tile_iso(
    0,
    RGBA:new{r = 1, g = 1, b = 1, a = 1},
    TILE_LAYERS.gui,
    Vec3:new{x = self.mouse.w_pos:floor().x, y = self.mouse.w_pos:floor().y, z = self.mouse.w_pos:snap(0.5).z},
    Vec3:new{x = self.camera.w_ofs.x, y = self.camera.w_ofs.y, z = self.camera.w_ofs.z}
  )
  self.tileset:draw_tile_iso(
    0,
    RGBA:new{r = 1, g = 1, b = 1, a = 0.5},
    TILE_LAYERS.gui,
    Vec3:new{x = self.mouse.w_pos:floor().x, y = self.mouse.w_pos:floor().y, z = 0.0},
    Vec3:new{x = self.camera.w_ofs.x, y = self.camera.w_ofs.y, z = self.camera.w_ofs.z}
  )
  if self.state.action ~= EDITOR_ACTIONS.none then
    self.tileset:draw_tile_iso(
      1,
      RGBA:new{r = 1, g = 1, b = 1, a = 0.5},
      TILE_LAYERS.gui,
      Vec3:new{x = self.mouse.w_pos:floor().x, y = self.mouse.w_pos:floor().y, z = self.mouse.w_pos:snap(0.5).z},
      Vec3:new{x = self.camera.w_ofs.x, y = self.camera.w_ofs.y, z = self.camera.w_ofs.z}
    )
  end

  -- render isometric preview
  -- tile mode
  if self.state.mode == EDITOR_MODES.tile and self.state.tileset_ref ~= nil then
    self.state.tileset_ref:draw_tile_iso(
      self.state.tile_index,
      RGBA:new{r = 1, g = 1, b = 1, a = 0.5},
      TILE_LAYERS.gui,
      Vec3:new{x = self.mouse.w_pos:floor().x, y = self.mouse.w_pos:floor().y, z = self.mouse.w_pos:snap(0.5).z},
      Vec3:new{x = self.camera.w_ofs.x, y = self.camera.w_ofs.y, z = self.camera.w_ofs.z}
    )

  -- entity mode
  elseif self.state.mode == EDITOR_MODES.entity and self.state.spritesheet_ref ~= nil then
    self.state.spritesheet_ref:draw_sprite_iso(
      1,
      RGBA:new{
        r = self.state.entity_data_ref.color.r,
        g = self.state.entity_data_ref.color.g,
        b = self.state.entity_data_ref.color.b,
        a = 0.5
      },
      SPRITE_LAYERS.gui,
      Vec3:new{x = self.mouse.w_bbox.pos_x, y = self.mouse.w_bbox.pos_y, z = self.mouse.w_pos:snap(0.5).z},
      Vec3:new{x = self.camera.w_ofs.x, y = self.camera.w_ofs.y, z = self.camera.w_ofs.z}
    )

  -- object mode
  elseif self.state.mode == EDITOR_MODES.object and self.state.spritesheet_ref ~= nil then
    self.state.spritesheet_ref:draw_sprite_iso(
      1,
      RGBA:new{
        r = self.state.object_data_ref.color.r,
        g = self.state.object_data_ref.color.g,
        b = self.state.object_data_ref.color.b,
        a = 0.5
      },
      SPRITE_LAYERS.gui,
      Vec3:new{x = self.mouse.w_bbox.pos_x, y = self.mouse.w_bbox.pos_y, z = self.mouse.w_pos:snap(0.5).z},
      Vec3:new{x = self.camera.w_ofs.x, y = self.camera.w_ofs.y, z = self.camera.w_ofs.z}
    )
  end

  -- render all isometric graphics
  IsoRender.execute_render_cmds()

  -- render gui
  self.gui:render()
end
