require "common.base"
local Slab = require "3rdparty.slab.Slab"
local json = require "3rdparty.json"

GUI = setmetatable({
  class_name = "GUI",
  editor_ref = nil,
  style = nil,
  hovered = nil,
  load_file = nil,
  save_file = nil,
  map_width = nil,
  map_height = nil
}, {__index = Base})

function GUI:new(instance)
  instance = instance or {}
  setmetatable(instance, self)
  self.__index = self
  return instance:init()
end

function GUI:init()
  Slab.Initialize()
  self.style = Slab.GetStyle()
  self.style.API.SetStyle("Default")
  self.hovered = false
  self.load_file = false
  self.save_file = false
  self.map_width = 8
  self.map_height = 8

  return self
end

function GUI:update(dt)
  Slab.Update(dt)

  -- main menu
  if Slab.BeginMainMenuBar() then
    -- file
    if Slab.BeginMenu("File") then
      -- new map
      if Slab.MenuItem("New") then
        self.editor_ref.map = Map:new{}
      end

      -- load map
      if Slab.MenuItem("Load") then
        self.load_file = true
      end

      -- save map
      if Slab.MenuItem("Save") then
        self.save_file = true
      end

      Slab.Separator()

      -- exit
      if Slab.MenuItem("Exit") then
        love.event.quit(0)
      end

      Slab.EndMenu()
    end

    -- map
    if Slab.BeginMenu("Map") then
      -- resize
      if Slab.MenuItem("Resize") then
        Slab.OpenDialog("map-resize")
      end

      Slab.EndMenu()
    end

    -- editor
    if Slab.BeginMenu("Editor") then
      -- cell mode
      if Slab.MenuItemChecked("Cell mode", self.editor_ref.state.mode == EDITOR_MODES.cell) then
        self.editor_ref.state.mode = EDITOR_MODES.cell
      end

      -- cell flag
      if Slab.BeginMenu("Cell flag") then
        for cf_key, cf_val in pairs(CELL_FLAGS) do
          if Slab.MenuItemChecked(cf_val .. ": " .. cf_key, self.editor_ref.state.cell_flag == cf_val) then
            self.editor_ref.state.cell_flag = cf_val
          end
        end

        Slab.EndMenu()
      end

      Slab.Separator()

      -- tile mode
      if Slab.MenuItemChecked("Tile mode", self.editor_ref.state.mode == EDITOR_MODES.tile) then
        self.editor_ref.state.mode = EDITOR_MODES.tile
      end

      -- tileset
      if Slab.BeginMenu("Tileset") then
        for _, ti_val in pairs(TILESET_INDEX) do
          if Slab.MenuItemChecked(ti_val.index .. ": " .. ti_val.identifier, self.editor_ref.state.tileset_index == ti_val.index) then
            self.editor_ref.state.tileset_index = ti_val.index
          end
        end

        Slab.EndMenu()
      end

      -- tile layer
      if Slab.BeginMenu("Tile layer") then
        for tl_key, tl_val in pairs(TILE_LAYERS) do
          if Slab.MenuItemChecked(tl_val .. ": " .. tl_key, self.editor_ref.state.tile_layer == tl_val) then
            self.editor_ref.state.tile_layer = tl_val
          end
        end

        Slab.EndMenu()
      end

      Slab.Separator()

      -- entity mode
      if Slab.MenuItemChecked("Entity mode", self.editor_ref.state.mode == EDITOR_MODES.entity) then
        self.editor_ref.state.mode = EDITOR_MODES.entity
      end

      Slab.Separator()

      -- object mode
      if Slab.MenuItemChecked("Object mode", self.editor_ref.state.mode == EDITOR_MODES.object) then
        self.editor_ref.state.mode = EDITOR_MODES.object
      end

      Slab.EndMenu()
    end

    Slab.EndMainMenuBar()
  end

  -- load map dialog
  if self.load_file then
    local result = Slab.FileDialog({
      AllowMultiSelect = false,
      Directory = "./res/maps",
      Type = "openfile",
      Filters = {{"*.json", "JSON files"}}
    })

    if result.Button == "OK" then
      self.load_file = false
      -- vars
      local file_path = result.Files[1]
      -- load map
      self.editor_ref.map.file_path = file_path
      self.editor_ref.map:load()
      -- update vars
      self.map_width = self.editor_ref.map.width
      self.map_height = self.editor_ref.map.height
    elseif result.Button == "Cancel" then
      self.load_file = false
    end
  end

  -- save map dialog
  if self.save_file then
    local result = Slab.FileDialog({
      AllowMultiSelect = false,
      Directory = "./res/maps",
      Type = "savefile"
    })

    if result.Button == "OK" then
      self.save_file = false
      -- vars
      local file_path = result.Files[1]
      -- save map
      self.editor_ref.map.file_path = file_path
      self.editor_ref.map:save()
    elseif result.Button == "Cancel" then
      self.save_file = false
    end
  end

  -- set map dimensions dialog
  if Slab.BeginDialog("map-resize", {
    Title = "Resize the map",
    TitleAlignX = "left"
  }) then
    Slab.BeginLayout("map-resize-layout", {
      AlignX = "left",
      AlignY = "top",
      ExpandW = true
    })

    -- width
    if Slab.Input("map-resize-input-w", {
      Text = tostring(self.map_width),
      NumbersOnly = true,
      MinNumber = 1,
      MaxNumber = 256,
      Step = 1,
      UseSlider = true
    }) then
      self.map_width = math.floor(Slab.GetInputNumber())
    end

    -- height
    if Slab.Input("map-resize-input-h", {
      Text = tostring(self.map_height),
      NumbersOnly = true,
      MinNumber = 1,
      MaxNumber = 256,
      Step = 1,
      UseSlider = true
    }) then
      self.map_height = math.floor(Slab.GetInputNumber())
    end

    -- ok
    if Slab.Button("OK") then
      Slab.CloseDialog("map-resize")
      -- data
      local m_data = self.editor_ref.map:export_data()
      -- init temp map with new size to get calculated dims for new size
      local temp_map = Map:new{
        width = self.map_width,
        height = self.map_height
      }
      -- modify data
      m_data.width = temp_map.width
      m_data.height = temp_map.height
      m_data.size = temp_map.size
      m_data.bbox = {
        min_x = temp_map.bbox.min_x,
        min_y = temp_map.bbox.min_y,
        max_x = temp_map.bbox.max_x,
        max_y = temp_map.bbox.max_y,
      }
      -- import modified data back to our map
      self.editor_ref.map:import_data(m_data)
    end

    -- cancel
    if Slab.Button("Cancel") then
      Slab.CloseDialog("map-resize")
    end

    Slab.EndLayout()
    Slab.EndDialog()
  end

  -- tile selector
  -- TODO: for this we need to optimize tileset sizes so that there is not y-gap between tiles
  -- local tileset_ref = self.editor_ref.state.tileset_ref
  -- if self.editor_ref.state.mode == EDITOR_MODES.tile and tileset_ref ~= nil then
  --   Slab.BeginWindow("tile-selector", {
  --     Title = "Tile selector",
  --     TitleAlignX = "left",
  --     AutoSizeWindow = false,
  --     NoSavedSettings = true
  --   })
  --   Slab.BeginLayout("tile-selector-layout", {
  --     AlignX = "left",
  --     AlignY = "top",
  --     ExpandW = true,
  --     ExpandH = true
  --   })

  --   Slab.BeginListBox("tile-selector-list")
  --   for x = 0, tileset_ref.width - 1 do
  --     for y = 0, tileset_ref.height - 1 do
  --       local index = x + y * tileset_ref.width
  --       local sub_x = x * tileset_ref.tile_w + (x * tileset_ref.x_skip)
  --       local sub_y = y * tileset_ref.tile_h + (y * tileset_ref.y_skip)
  --       local sub_w = tileset_ref.tile_w - tileset_ref.tile_c_x
  --       local sub_h = tileset_ref.tile_h - tileset_ref.tile_c_y

  --       print(string.format("x: %d, y: %d, w: %d, h: %d", sub_x, sub_y, sub_w, sub_h))

  --       Slab.BeginListBoxItem("tile-selector-list-item-" .. index)
  --       Slab.Image("tile-selector-list-item-" .. index .. "-image", {
  --         Image = tileset_ref.image,
  --         SubX = sub_x,
  --         SubY = sub_y,
  --         SubW = sub_w,
  --         SubH = sub_h
  --       })
  --       Slab.SameLine({CenterY = true})
  --       Slab.Text("Tile index " .. index)
  --       Slab.EndListBoxItem()
  --     end
  --   end
  --   Slab.EndListBox()

  --   Slab.EndLayout()
  --   Slab.EndWindow()
  -- end

  -- update hovered
  self.hovered = Slab.IsVoidHovered() == false
end

function GUI:render()
  Slab.Draw()
end

function GUI:select_object(object)

end
