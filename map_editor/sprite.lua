require "common.base"
require "common.sprite"

Sprite = setmetatable({
  class_name = "Sprite",
  spritesheet_index = nil,
  index = nil,
  layer = nil,
  color = nil
}, {__index = Base})

function Sprite:new(instance)
  instance = instance or {}
  setmetatable(instance, self)
  self.__index = self
  return instance:init()
end

function Sprite:init()
  self.spritesheet_index = ObjUtil.set_if_nil(self.spritesheet_index, nil)
  self.index = ObjUtil.set_if_nil(self.index, nil)
  self.layer = ObjUtil.set_if_nil(self.layer, SPRITE_LAYERS.entity)
  self.color = ObjUtil.set_if_nil(self.color, RGBA:new{r = 1, g = 1, b = 1, a = 1})

  return self
end
