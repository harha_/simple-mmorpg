require "common.base"
require "common.object_data"
require "map_editor.sprite"

Object = setmetatable({
  class_name = "Object",
  object_data_index = nil,
  data = nil,
  sprite = nil,
  x = nil,
  y = nil,
  custom_data = nil
}, {__index = Base})

function Object:new(instance)
  instance = instance or {}
  setmetatable(instance, self)
  self.__index = self
  return instance:init()
end

function Object:init()
  self.object_data_index = ObjUtil.set_if_nil(self.object_data_index, 1)
  self.data = ObjectData.load(self.object_data_index)
  self.sprite = ObjUtil.set_if_nil(self.sprite, Sprite:new{
    spritesheet_index = self.data.spritesheet_indexes.default,
    index = 0,
    layer = SPRITE_LAYERS.object,
    color = RGBA:new{r = self.data.color.r, g = self.data.color.g, b = self.data.color.b, a = self.data.color.a}
  })
  self.x = ObjUtil.set_if_nil(self.x, 0)
  self.y = ObjUtil.set_if_nil(self.y, 0)
  self.custom_data = ObjUtil.set_if_nil(self.custom_data, "")

  return self
end
