require "common.base"
require "common.cell"
require "common.bbox"

Cell = setmetatable({
  class_name = "Cell",
  x = nil,
  y = nil,
  z = nil,
  bbox = nil,
  flag = nil,
  tiles = nil,
}, {__index = Base})

function Cell:new(instance)
  instance = instance or {}
  setmetatable(instance, self)
  self.__index = self
  return instance:init()
end

function Cell:init()
  self.x = ObjUtil.set_if_nil(self.x, 0)
  self.y = ObjUtil.set_if_nil(self.y, 0)
  self.z = ObjUtil.set_if_nil(self.z, 0)
  self.bbox = ObjUtil.set_if_nil(self.bbox, BBox:new{
    min_x = self.x,
    min_y = self.y,
    max_x = self.x + 1,
    max_y = self.y + 1
  })
  self.flag = ObjUtil.set_if_nil(self.flag, CELL_FLAGS.walk)
  self.tiles = ObjUtil.set_if_nil(self.tiles, {})

  return self
end
