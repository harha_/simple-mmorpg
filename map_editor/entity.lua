require "common.base"
require "common.entity_data"
require "map_editor.sprite"

Entity = setmetatable({
  class_name = "Entity",
  entity_data_index = nil,
  data = nil,
  sprite = nil,
  x = nil,
  y = nil
}, {__index = Base})

function Entity:new(instance)
  instance = instance or {}
  setmetatable(instance, self)
  self.__index = self
  return instance:init()
end

function Entity:init()
  self.entity_data_index = ObjUtil.set_if_nil(self.entity_data_index, 1)
  self.data = EntityData.load(self.entity_data_index)
  self.sprite = ObjUtil.set_if_nil(self.sprite, Sprite:new{
    spritesheet_index = self.data.spritesheet_indexes.idle,
    index = 0,
    layer = SPRITE_LAYERS.entity,
    color = RGBA:new{r = self.data.color.r, g = self.data.color.g, b = self.data.color.b, a = self.data.color.a}
  })
  self.x = ObjUtil.set_if_nil(self.x, 0)
  self.y = ObjUtil.set_if_nil(self.y, 0)

  return self
end
