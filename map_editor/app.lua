require "common.base"
require "graphics.animationsheet"
require "graphics.tileset"
require "graphics.spritesheet"
require "common.entity_data"
require "common.object_data"
require "map_editor.editor"

App = {
  editor = nil
}

function App.load()
  -- setup window
  love.window.setMode(1280, 720, {resizable=false})
  love.window.setTitle("map_editor")
  love.window.setFullscreen(false)

  -- setup input
  love.keyboard.setKeyRepeat(true)

  -- load data
  Animationsheet.load_index()
  Tileset.load_index()
  Spritesheet.load_index()
  EntityData.load_index()
  ObjectData.load_index()

  -- load editor
  App.editor = Editor:new{}
end

function love.textinput(text)
  App.editor:textinput(text)
end

function love.keypressed(key, scancode, isrepeat)
  App.editor:keypressed(key, scancode, isrepeat)
end

function love.keyreleased(key, scancode, isrepeat)
  App.editor:keyreleased(key, scancode, isrepeat)
end

function love.wheelmoved(x, y)
  App.editor:wheelmoved(x, y)
end

function love.mousepressed(x, y, button, istouch, presses)
  App.editor:mousepressed(x, y, button, istouch, presses)
end

function love.mousereleased(x, y, button, istouch, presses)
  App.editor:mousereleased(x, y, button, istouch, presses)
end

function love.mousemoved(x, y, dx, dy, istouch)
  App.editor:mousemoved(x, y, dx, dy, istouch)
end

function love.update(dt)
  App.editor:update(dt)
end

function love.draw()
  App.editor:render()
end
