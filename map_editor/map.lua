require "common.base"
require "common.bbox"
require "map_editor.cell"
require "map_editor.tile"
require "map_editor.entity"
require "map_editor.object"
require "graphics.rgba"
require "graphics.tileset"
require "graphics.spritesheet"
local json = require "3rdparty.json"

Map = setmetatable({
  class_name = "Map",
  file_path = nil,
  width = nil,
  height = nil,
  size = nil,
  bbox = nil,
  cells = nil,
  entities = nil,
  objects = nil
}, {__index = Base})

function Map:new(instance)
  instance = instance or {}
  setmetatable(instance, self)
  self.__index = self
  return instance:init()
end

function Map:init()
  self.file_path = ObjUtil.set_if_nil(self.file_path, "res/maps/new.json")
  self.width = ObjUtil.set_if_nil(self.width, 8)
  self.height = ObjUtil.set_if_nil(self.height, 8)
  self.size = self.width * self.height
  self.bbox = ObjUtil.set_if_nil(self.bbox, BBox:new{
    min_x = 0,
    min_y = 0,
    max_x = self.width,
    max_y = self.height
  })
  self.cells = ObjUtil.set_if_nil(self.cells, {})
  self.entities = ObjUtil.set_if_nil(self.entities, {})
  self.objects = ObjUtil.set_if_nil(self.objects, {})

  return self
end

function Map:export_data()
  local data = {
    file_path = self.file_path,
    width = self.width,
    height = self.height,
    size = self.size,
    bbox = {
      min_x = self.bbox.min_x,
      min_y = self.bbox.min_y,
      max_x = self.bbox.max_x,
      max_y = self.bbox.max_y
    },
    cells = {},
    entities = {},
    objects = {}
  }

  for _, c in pairs(self.cells) do
    if c ~= nil then
      local d = {
        x = c.x,
        y = c.y,
        z = c.z,
        bbox = {
          min_x = c.bbox.min_x,
          min_y = c.bbox.min_y,
          max_x = c.bbox.max_x,
          max_y = c.bbox.max_y
        },
        flag = c.flag,
        tiles = {}
      }
      for _, t in pairs(c.tiles) do
        table.insert(d.tiles, {
          tileset_index = t.tileset_index,
          index = t.index,
          layer = t.layer
        })
      end
      table.insert(data.cells, d)
    end
  end

  for _, e in pairs(self.entities) do
    if e ~= nil then
      table.insert(data.entities, {
        entity_data_index = e.entity_data_index,
        x = e.x,
        y = e.y
      })
    end
  end

  for _, o in pairs(self.objects) do
    if o ~= nil then
      table.insert(data.objects, {
        object_data_index = o.object_data_index,
        x = o.x,
        y = o.y,
        custom_data = o.custom_data
      })
    end
  end

  return data
end

function Map:import_data(data)
  self.file_path = data.file_path
  self.width = data.width
  self.height = data.height
  self.size = data.size
  self.bbox = BBox:new{
    min_x = data.bbox.min_x,
    min_y = data.bbox.min_y,
    max_x = data.bbox.max_x,
    max_y = data.bbox.max_y
  }
  self.cells = {}

  for i = 1,#data.cells do
    local d = data.cells[i]
    local c = Cell:new{
      x = d.x,
      y = d.y,
      z = d.z,
      bbox = nil,
      flag = d.flag,
      tiles = {}
    }
    for j= 1,#d.tiles do
      local dt = d.tiles[j]
      c.tiles[dt.layer] = Tile:new{
        tileset_index = dt.tileset_index,
        index = dt.index,
        layer = dt.layer
      }
    end

    if c.x < self.width and c.y < self.height then
      self.cells[c.x + c.y * self.width] = c
    end
  end

  self.entities = {}

  for i = 1,#data.entities do
    local d = data.entities[i]
    local e = Entity:new{
      entity_data_index = d.entity_data_index,
      x = d.x,
      y = d.y
    }

    if math.floor(e.x) < self.width and math.floor(e.y) < self.height then
      self.entities[math.floor(e.x) + math.floor(e.y) * self.width] = e
    end
  end

  self.objects = {}

  for i = 1,#data.objects do
    local d = data.objects[i]
    local o = Object:new{
      object_data_index = d.object_data_index,
      x = d.x,
      y = d.y,
      custom_data = d.custom_data
    }

    if math.floor(o.x) < self.width and math.floor(o.y) < self.height then
      self.objects[math.floor(o.x) + math.floor(o.y) * self.width] = o
    end
  end
end

function Map:save()
  local file_ptr = assert(io.open(self.file_path, "wb"))

  -- write data
  file_ptr:write(json.encode(self:export_data()))

  io.close(file_ptr)

  print("saved map to disk: " .. self.file_path)
end

function Map:load()
  local file_ptr = assert(io.open(self.file_path, "rb"))

  -- read data
  local file_dta = file_ptr:read("*all")

  io.close(file_ptr)

  -- import data
  self:import_data(json.decode(file_dta))

  print("loaded map into memory: " .. self.file_path)
end

function Map:render(ofs_x, ofs_y, ofs_z)
  for _, c in pairs(self.cells) do
    if c ~= nil then
      -- render cell flag indicator
      if c.flag == CELL_FLAGS.block then
        local tileset_editor = Tileset.load(1)
        tileset_editor:draw_tile_iso(
          1,
          RGBA:new{r = 1, g = 0, b = 0, a = 0.25},
          TILE_LAYERS.gui,
          Vec3:new{x = c.x, y = c.y, z = c.z},
          Vec3:new{x = ofs_x, y = ofs_y, z = ofs_z}
        )
      end
      -- render the tile itself
      for _, t in pairs(c.tiles) do
        local tileset = Tileset.load(t.tileset_index)
        tileset:draw_tile_iso(
          t.index,
          RGBA:new{r = 1, g = 1, b = 1, a = 1},
          t.layer,
          Vec3:new{x = c.x, y = c.y, z = c.z},
          Vec3:new{x = ofs_x, y = ofs_y, z = ofs_z}
        )
      end
    end
  end

  for _, e in pairs(self.entities) do
    if e ~= nil then
      local cell = self:get_cell_by_xy(e.x, e.y)
      local pos_z = 0.0
      if cell ~= nil then
        pos_z = cell.z
      end
      local spritesheet = Spritesheet.load(e.sprite.spritesheet_index)
      spritesheet:draw_sprite_iso(
        e.sprite.index,
        e.sprite.color,
        e.sprite.layer,
        Vec3:new{x = e.x, y = e.y, z = pos_z},
        Vec3:new{x = ofs_x, y = ofs_y, z = ofs_z}
      )
    end
  end

  for _, o in pairs(self.objects) do
    if o ~= nil then
      local cell = self:get_cell_by_xy(o.x, o.y)
      local pos_z = 0.0
      if cell ~= nil then
        pos_z = cell.z
      end
      local spritesheet = Spritesheet.load(o.sprite.spritesheet_index)
      spritesheet:draw_sprite_iso(
        o.sprite.index,
        o.sprite.color,
        o.sprite.layer,
        Vec3:new{x = o.x, y = o.y, z = pos_z},
        Vec3:new{x = ofs_x, y = ofs_y, z = ofs_z}
      )
    end
  end
end

function Map:get_cell_by_xy(x, y)
  if x < 0 or x >= self.width or y < 0 or y >= self.height then return nil end

  x = math.floor(x)
  y = math.floor(y)

  local index = x + y * self.width
  return self.cells[index]
end
function Map:set_cell(x, y, z, cell_flag)
  if x < 0 or x >= self.width or y < 0 or y >= self.height then return end

  x = math.floor(x)
  y = math.floor(y)
  z = MathUtil.snap(z, 0.5)

  local index = x + y * self.width
  -- new cell
  if self.cells[index] == nil then
    self.cells[index] = Cell:new{
      x = x,
      y = y,
      z = z,
      flag = cell_flag
    }
  -- existing cell
  else
    self.cells[index].z = z
    self.cells[index].flag = cell_flag
  end
end

function Map:erase_cell(x, y)
  if x < 0 or x >= self.width or y < 0 or y >= self.height then return end

  x = math.floor(x)
  y = math.floor(y)

  local index = x + y * self.width
  self.cells[index] = nil
end

function Map:set_tile(x, y, z, tileset_index, tile_index, tile_layer)
  if x < 0 or x >= self.width or y < 0 or y >= self.height then return end

  x = math.floor(x)
  y = math.floor(y)
  z = MathUtil.snap(z, 0.5)

  local index = x + y * self.width
  -- init new cell if does not exist
  if self.cells[index] == nil then
    self:set_cell(x, y, z, CELL_FLAGS.walk)
  end

  -- update cell z
  self.cells[index].z = z

  -- set tile layer data
  self.cells[index].tiles[tile_layer] = Tile:new{
    tileset_index = tileset_index,
    index = tile_index,
    layer = tile_layer
  }
end

function Map:erase_tile(x, y, tile_layer)
  if x < 0 or x >= self.width or y < 0 or y >= self.height then return end

  x = math.floor(x)
  y = math.floor(y)

  local index = x + y * self.width
  -- erase tile layer data
  if self.cells[index] ~= nil then
    self.cells[index].tiles[tile_layer] = nil
  end
end

function Map:set_entity(x, y, entity_data_index)
  if x < 0 or x >= self.width or y < 0 or y >= self.height then return end

  x = math.floor(x)
  y = math.floor(y)

  local index = x + y * self.width
  self.entities[index] = Entity:new{
    entity_data_index = entity_data_index,
    x = x + 0.5,
    y = y + 0.5
  }
end

function Map:erase_entity(x, y)
  if x < 0 or x >= self.width or y < 0 or y >= self.height then return end

  x = math.floor(x)
  y = math.floor(y)

  local index = x + y * self.width
  self.entities[index] = nil
end

function Map:get_object_by_xy(x, y)
  if x < 0 or x >= self.width or y < 0 or y >= self.height then return nil end

  x = math.floor(x)
  y = math.floor(y)

  local index = x + y * self.width
  return self.objects[index]
end

function Map:set_object(x, y, object_data_index, custom_data)
  if x < 0 or x >= self.width or y < 0 or y >= self.height then return end

  x = math.floor(x)
  y = math.floor(y)

  local index = x + y * self.width
  self.objects[index] = Object:new{
    object_data_index = object_data_index,
    x = x + 0.5,
    y = y + 0.5,
    custom_data = custom_data
  }
end

function Map:erase_object(x, y)
  if x < 0 or x >= self.width or y < 0 or y >= self.height then return end

  x = math.floor(x)
  y = math.floor(y)

  local index = x + y * self.width
  self.objects[index] = nil
end
