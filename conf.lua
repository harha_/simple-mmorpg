require "common.array_util"

local is_headless = false
for _, a in ipairs(arg) do
  if ArrayUtil.contains({'--server', '--client_sim'}, a) then
    is_headless = true
    break
  end
end

function love.conf(t)
  if is_headless then
    t.modules.audio = false
    t.modules.font = false
    t.modules.graphics = false
    t.modules.image = false
    t.modules.joystick = false
    t.modules.keyboard = false
    t.modules.mouse = false
    t.modules.sound = false
    t.modules.video = false
    t.modules.window = false

    t.window = nil
  end
end
