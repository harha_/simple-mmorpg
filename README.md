# Simple MMORPG

Simple base template for an isometric MMORPG, written in lua with the help of Love2D+json.lua+astar.lua+slab.

## Screenshots

![client](docs/screenshots/client.png "Client")
![editor](docs/screenshots/editor.png "Map editor")

## Launch arguments

```
--server
--client
--client_sim
--editor
```

## Example (launch server)

```bash
love . --server
```
