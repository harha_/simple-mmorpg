require "common.base"
require "common.obj_util"

BBox = setmetatable({
  class_name = "BBox",
  min_x = nil,
  min_y = nil,
  max_x = nil,
  max_y = nil,
  pos_x = nil,
  pos_y = nil,
  dim_x = nil,
  dim_y = nil
}, {__index = Base})

function BBox:new(instance)
  instance = instance or {}
  setmetatable(instance, self)
  self.__index = self
  return instance:init()
end

function BBox:init()
  self.min_x = ObjUtil.set_if_nil(self.min_x, 0)
  self.min_y = ObjUtil.set_if_nil(self.min_y, 0)
  self.max_x = ObjUtil.set_if_nil(self.max_x, 1)
  self.max_y = ObjUtil.set_if_nil(self.max_y, 1)
  self:getPos()
  self:getDim()

  return self
end

function BBox:__tostring()
  return string.format("BBox(min_x=%.4f, min_y=%.4f, max_x=%.4f, max_y=%.4f)", self.min_x, self.min_y, self.max_x, self.max_y)
end

function BBox:intersect_point(point_x, point_y)
  local dx = point_x - self.pos_x
  local px = self.dim_x * 0.5 - math.abs(dx)

  if px <= 0 then
    return nil
  end

  local dy = point_y - self.pos_y
  local py = self.dim_y * 0.5 - math.abs(dy)

  if py <= 0 then
    return nil
  end

  local hit = {
    delta_x = 0,
    delta_y = 0,
    normal_x = 0,
    normal_y = 0,
    pos_x = 0,
    pos_y = 0
  }
  if px < py then
    local sx = 0
    if dx > 0 then
      sx = 1
    elseif dx < 0 then
      sx = -1
    end

    hit.delta_x = px * sx
    hit.normal_x = sx
    hit.pos_x = self.pos_x + (self.dim_x * 0.5 * sx)
    hit.pos_y = point_y
  else
    local sy = 0
    if dy > 0 then
      sy = 1
    elseif dy < 0 then
      sy = -1
    end

    hit.delta_y = py * sy
    hit.normal_y = sy
    hit.pos_x = point_x
    hit.pos_y = self.pos_y + (self.dim_y * 0.5 * sy)
  end

  return hit
end

function BBox:intersect_bbox(bbox)
  local half_x = self.dim_x * 0.5
  local half_y = self.dim_y * 0.5
  local bbox_half_x = bbox.dim_x * 0.5
  local bbox_half_y = bbox.dim_y * 0.5

  local dx = bbox.pos_x - self.pos_x
  local px = (half_x + bbox_half_x) - math.abs(dx)

  if px <= 0 then
    return nil
  end

  local dy = bbox.pos_y - self.pos_y
  local py = (half_y + bbox_half_y) - math.abs(dy)

  if py <= 0 then
    return nil
  end

  local hit = {
    delta_x = 0,
    delta_y = 0,
    normal_x = 0,
    normal_y = 0,
    pos_x = 0,
    pos_y = 0
  }
  if px < py then
    local sx = 0
    if dx > 0 then
      sx = 1
    elseif dx < 0 then
      sx = -1
    end

    hit.delta_x = px * sx
    hit.normal_x = sx
    hit.pos_x = self.pos_x + (half_x * sx)
    hit.pos_y = bbox.pos_y
  else
    local sy = 0
    if dy > 0 then
      sy = 1
    elseif dy < 0 then
      sy = -1
    end

    hit.delta_y = py * sy
    hit.normal_y = sy
    hit.pos_x = bbox.pos_x
    hit.pos_y = self.pos_y + (half_y * sy)
  end

  return hit
end

function BBox:getMin()
  self.min_x = self.pos_x - self.dim_x * 0.5
  self.min_y = self.pos_y - self.dim_y * 0.5
  return self.min_x, self.min_y
end

function BBox:getMax()
  self.max_x = self.pos_x + self.dim_x * 0.5
  self.max_y = self.pos_y + self.dim_y * 0.5
  return self.max_x, self.max_y
end

function BBox:getPos()
  self.pos_x = (self.max_x + self.min_x) * 0.5
  self.pos_y = (self.max_y + self.min_y) * 0.5
  return self.pos_x, self.pos_y
end

function BBox:getDim()
  self.dim_x = self.max_x - self.min_x
  self.dim_y = self.max_y - self.min_y
  return self.dim_x, self.dim_y
end

function BBox:getSize()
  return (self.dim_x + self.dim_y) * 0.5 * 0.5
end
