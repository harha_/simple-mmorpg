require "common.base"
local json = require "3rdparty.json"

ENTITY_DATA_INDEX = {}
ENTITY_DATA_LOADED = {}

EntityData = setmetatable({
  class_name = "EntityData",
  index = nil,
  identifier = nil,
  file_path = nil,
  spritesheet_indexes = nil,
  sound_indexes = nil,
  color = nil,
  size = nil,
  speed = nil,
  stats = nil,
  ai = nil
}, {__index = Base})

function EntityData.load_index()
  -- read data
  local file_ptr = assert(io.open("res/entity_data_index.json", "rb"))
  local data = json.decode(file_ptr:read("*all"))
  io.close(file_ptr)

  -- store into memory
  for k, v in ipairs(data) do
    ENTITY_DATA_INDEX[v.index] = v
  end

  print("loaded entity data index into memory...")
end

function EntityData.load(index)
  local ref = ENTITY_DATA_LOADED[index]

  -- load existing
  if ref ~= nil then
    return ref
  end

  -- load new
  ENTITY_DATA_LOADED[index] = EntityData:new{index = index}

  return ENTITY_DATA_LOADED[index]
end

function EntityData:new(instance)
  instance = instance or {}
  setmetatable(instance, self)
  self.__index = self
  return instance:init()
end

function EntityData:init()
  local entitydata = ENTITY_DATA_INDEX[self.index]

  self.identifier = entitydata.identifier
  self.file_path = entitydata.file_path

  -- read data
  local file_ptr = assert(io.open(self.file_path, "rb"))
  local data = json.decode(file_ptr:read("*all"))
  io.close(file_ptr)

  -- import data
  self.spritesheet_indexes = data.spritesheet_indexes
  self.sound_indexes = data.sound_indexes
  self.color = data.color
  self.size = data.size
  self.speed = data.speed
  self.stats = data.stats
  self.ai = data.ai

  print("loaded new entity data into memory: " .. self.file_path)

  return self
end
