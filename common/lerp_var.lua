require "common.base"
require "common.obj_util"
require "common.math_util"

LerpVar = setmetatable({
  class_name = "LerpVar",
  factor = nil,
  time = nil,
  val = nil,
  val_initial = nil,
  val_target = nil
}, {__index = Base})

function LerpVar:new(instance)
  instance = instance or {}
  setmetatable(instance, self)
  self.__index = self
  return instance:init()
end

function LerpVar:init()
  self.factor = ObjUtil.set_if_nil(self.factor, 1)
  self.time = self.factor
  self.val = ObjUtil.set_if_nil(self.val, 0)
  self.val_initial = ObjUtil.set_if_nil(self.val_initial, 0)
  self.val_target = ObjUtil.set_if_nil(self.val_target, 0)

  return self
end

function LerpVar:update(dt)
  if self.time <= self.factor then
    self.val = MathUtil.lerpf(self.val_initial, self.val_target, self.time / self.factor)
  end
  self.time = self.time + dt
end

function LerpVar:set_target(val_initial, val_target)
  if val_target == self.val_target then return end

  self.time = 0.0
  self.val = val_initial
  self.val_initial = val_initial
  self.val_target = val_target
end

function LerpVar:is_active()
  return self.time <= self.factor
end
