require "common.base"
local json = require "3rdparty.json"

OBJECT_DATA_INDEX = {}
OBJECT_DATA_LOADED = {}

ObjectData = setmetatable({
  class_name = "ObjectData",
  index = nil,
  identifier = nil,
  file_path = nil,
  spritesheet_indexes = nil,
  sound_indexes = nil,
  size = nil,
  color = nil
}, {__index = Base})

function ObjectData.load_index()
  -- read data
  local file_ptr = assert(io.open("res/object_data_index.json", "rb"))
  local data = json.decode(file_ptr:read("*all"))
  io.close(file_ptr)

  -- store into memory
  for k, v in ipairs(data) do
    OBJECT_DATA_INDEX[v.index] = v
  end

  print("loaded object data index into memory...")
end

function ObjectData.load(index)
  local ref = OBJECT_DATA_LOADED[index]

  -- load existing
  if ref ~= nil then
    return ref
  end

  -- load new
  OBJECT_DATA_LOADED[index] = ObjectData:new{index = index}

  return OBJECT_DATA_LOADED[index]
end

function ObjectData:new(instance)
  instance = instance or {}
  setmetatable(instance, self)
  self.__index = self
  return instance:init()
end

function ObjectData:init()
  local objectdata = OBJECT_DATA_INDEX[self.index]

  self.identifier = objectdata.identifier
  self.file_path = objectdata.file_path

  -- read data
  local file_ptr = assert(io.open(self.file_path, "rb"))
  local data = json.decode(file_ptr:read("*all"))
  io.close(file_ptr)

  -- import data
  self.spritesheet_indexes = data.spritesheet_indexes
  self.sound_indexes = data.sound_indexes
  self.size = data.size
  self.color = data.color

  print("loaded new object data into memory: " .. self.file_path)

  return self
end
