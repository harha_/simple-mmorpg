require "common.base"
require "common.obj_util"
require "common.math_util"

Vec3 = setmetatable({
  class_name = "Vec3",
  x = nil,
  y = nil,
  z = nil
}, {__index = Base})

function Vec3:new(instance)
  instance = instance or {}
  setmetatable(instance, self)
  self.__index = self
  return instance:init()
end

function Vec3:init()
  self.x = ObjUtil.set_if_nil(self.x, 0)
  self.y = ObjUtil.set_if_nil(self.y, 0)
  self.z = ObjUtil.set_if_nil(self.z, 0)

  return self
end

function Vec3:__tostring()
  return string.format("Vec3(x=%.4f, y=%.4f, z=%.4f)", self.x, self.y, self.z)
end

function Vec3:floor()
  return Vec3:new{
    x = math.floor(self.x),
    y = math.floor(self.y),
    z = math.floor(self.z)
  }
end

function Vec3:ceil()
  return Vec3:new{
    x = math.ceil(self.x),
    y = math.ceil(self.y),
    z = math.ceil(self.z)
  }
end

function Vec3:snap(g)
  return Vec3:new{
    x = MathUtil.snap(self.x, g),
    y = MathUtil.snap(self.y, g),
    z = MathUtil.snap(self.z, g)
  }
end

function Vec3:eq(v, epsilon)
  return self.x >= v.x - epsilon and self.x <= v.x + epsilon and
         self.y >= v.y - epsilon and self.y <= v.y + epsilon and
         self.z >= v.z - epsilon and self.z <= v.z + epsilon
end

function Vec3:mul(f)
  return Vec3:new{
    x = self.x * f,
    y = self.y * f,
    z = self.z * f
  }
end

function Vec3:length()
  return math.sqrt(self.x * self.x + self.y * self.y + self.z * self.z)
end

function Vec3:normalize()
  local l = 1.0 / self:length()

  return Vec3:new{
    x = self.x * l,
    y = self.y * l,
    z = self.z * l
  }
end

function Vec3:negate()
  return Vec3:new{
    x = -self.x,
    y = -self.y,
    z = -self.z
  }
end

function Vec3:__eq(v)
  return self.x == v.x and self.y == v.y and self.z == v.z
end

function Vec3:__add(v)
  return Vec3:new{
    x = self.x + v.x,
    y = self.y + v.y,
    z = self.z + v.z
  }
end

function Vec3:__sub(v)
  return Vec3:new{
    x = self.x - v.x,
    y = self.y - v.y,
    z = self.z - v.z
  }
end
