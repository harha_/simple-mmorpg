MathUtil = {}

function MathUtil.randomf(min, max)
  return min + math.random() * (max - min)
end

function MathUtil.lerpf(s, e, t)
  return s * (1.0 - t) + e * t;
end

function MathUtil.lerpv(s, e, t)
  return s:mul(1.0 - t) + e:mul(t);
end

function MathUtil.round(n, d)
  local mult = 10^(d or 0)
  return math.floor(n * mult + 0.5) / mult
end

function MathUtil.snap(n, g)
  return math.floor(n / g) * g
end
