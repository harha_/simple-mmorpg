local json = require "3rdparty.json"

Base = {
  class_name = "Base",
}

function Base:new(instance)
  instance = instance or {}
  setmetatable(instance, self)
  self.__index = self
  return instance
end

function Base:__tostring()
  return json.encode(self)
end
