require "common.base"
require "common.obj_util"
require "common.math_util"

Vec2 = setmetatable({
  class_name = "Vec2",
  x = nil,
  y = nil
}, {__index = Base})

function Vec2:new(instance)
  instance = instance or {}
  setmetatable(instance, self)
  self.__index = self
  return instance:init()
end

function Vec2:init()
  self.x = ObjUtil.set_if_nil(self.x, 0)
  self.y = ObjUtil.set_if_nil(self.y, 0)

  return self
end

function Vec2:__tostring()
  return string.format("Vec2(x=%.4f, y=%.4f)", self.x, self.y)
end

function Vec2:floor()
  return Vec2:new{
    x = math.floor(self.x),
    y = math.floor(self.y)
  }
end

function Vec2:ceil()
  return Vec2:new{
    x = math.ceil(self.x),
    y = math.ceil(self.y)
  }
end

function Vec2:snap(g)
  return Vec2:new{
    x = MathUtil.snap(self.x, g),
    y = MathUtil.snap(self.y, g)
  }
end

function Vec2:eq(v, epsilon)
  return self.x >= v.x - epsilon and self.x <= v.x + epsilon and
         self.y >= v.y - epsilon and self.y <= v.y + epsilon
end

function Vec2:mul(f)
  return Vec2:new{
    x = self.x * f,
    y = self.y * f
  }
end

function Vec2:length()
  return math.sqrt(self.x * self.x + self.y * self.y)
end

function Vec2:normalize()
  local l = 1.0 / self:length()

  return Vec2:new{
    x = self.x * l,
    y = self.y * l
  }
end

function Vec2:negate()
  return Vec2:new{
    x = -self.x,
    y = -self.y
  }
end

function Vec2:atan2()
  return math.atan2(self.y, self.x)
end

function Vec2:perpendicular_cw()
  return Vec2:new{
    x = self.y,
    y = -self.x
  }
end

function Vec2:perpendicular_ccw()
  return Vec2:new{
    x = -self.y,
    y = self.x
  }
end

function Vec2:__eq(v)
  return self.x == v.x and self.y == v.y
end

function Vec2:__add(v)
  return Vec2:new{
    x = self.x + v.x,
    y = self.y + v.y
  }
end

function Vec2:__sub(v)
  return Vec2:new{
    x = self.x - v.x,
    y = self.y - v.y
  }
end
