ArrayUtil = {}

function ArrayUtil.contains(array, search_val)
  for _, val in ipairs(array) do
    if val == search_val then return true end
  end

  return false
end
