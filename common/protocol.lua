PROTOCOL_PACKETS = {
  PING = -1,
  PONG = -2,

  LOGIN_REQ = 1,
  ENTITY_MOVE_REQ = 2,
  ENTITY_PATH_REQ = 3,
  ENTITY_MELEE_REQ = 4,
  ENTITY_MAGIC_REQ = 5,
  MESSAGE_REQ = 6,

  LOGIN_RSP = 1001,
  MAP_SYNC_RSP = 1002,
  CELL_SYNC_RSP = 1003,
  ENTITY_SYNC_RSP = 1004,
  ENTITY_DELETE_RSP = 1005,
  ENTITY_MOVE_RSP = 1006,
  ENTITY_MELEE_RSP = 1007,
  ENTITY_MAGIC_RSP = 1008,
  OBJECT_SYNC_RSP = 1009,
  OBJECT_DELETE_RSP = 1010,
  MESSAGE_RSP = 1011
}