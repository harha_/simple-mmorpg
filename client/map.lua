require "common.base"
require "common.bbox"
require "client.cell"
require "client.tile"
require "client.entity"
require "client.object"
require "graphics.iso_math"
require "graphics.rgba"
require "graphics.tileset"
require "graphics.spritesheet"
local json = require "3rdparty.json"

Map = setmetatable({
  class_name = "Map",
  client_ref = nil,
  width = nil,
  height = nil,
  size = nil,
  bbox = nil,
  cells = nil,
  entities = nil,
  objects = nil,
  screen_cells = nil,
  screen_entities = nil,
  screen_objects = nil,
  phys = nil,
  statistics = nil
}, {__index = Base})

function Map:new(instance)
  instance = instance or {}
  setmetatable(instance, self)
  self.__index = self
  return instance:init()
end

function Map:init()
  self.width = ObjUtil.set_if_nil(self.width, 4)
  self.height = ObjUtil.set_if_nil(self.height, 4)
  self.size = self.width * self.height
  self.bbox = BBox:new{
    min_x = 0,
    min_y = 0,
    max_x = self.width,
    max_y = self.height
  }
  self.cells = {}
  self.entities = {}
  self.objects = {}
  self.screen_cells = {}
  self.screen_entities = {}
  self.screen_objects = {}
  self.phys = nil
  self.statistics = {
    entity_n = 0,
    object_n = 0
  }

  return self
end

function Map:init_phys()
  if self.phys ~= nil then
    self.phys.world:destroy()
    self.phys.world = nil
    self.phys.body = nil
    self.phys.shape = nil
    self.phys.fixture = nil
  end

  love.physics.setMeter(1.0)
  self.phys = {
    world = love.physics.newWorld(0, 0, true),
    body = nil,
    shape = nil,
    fixture = nil
  }
  self.phys.body = love.physics.newBody(self.phys.world, self.bbox.min_x, self.bbox.min_y, "static")
  self.phys.shape = love.physics.newChainShape(true, 0, 0, self.width, 0, self.width, self.height, 0, self.height)
  self.phys.fixture = love.physics.newFixture(self.phys.body, self.phys.shape)
end

function Map:import_data(data)
  self.width = data.width
  self.height = data.height
  self.size = data.size
  self.bbox = BBox:new{
    min_x = data.bbox.min_x,
    min_y = data.bbox.min_y,
    max_x = data.bbox.max_x,
    max_y = data.bbox.max_y
  }

  self:init_phys()

  self.cells = {}
  self.entities = {}
  self.objects = {}
end

function Map:import_cell_data(data)
  for i = 1,#data do
    local d = data[i]
    local c = Cell:new{
      map_ref = self,
      x = d.x,
      y = d.y,
      z = d.z,
      flag = d.flag,
      tiles = {}
    }
    for j= 1,#d.tiles do
      local dt = d.tiles[j]
      c.tiles[dt.layer] = Tile:new{
        tileset_index = dt.tileset_index,
        index = dt.index,
        layer = dt.layer
      }
    end
    self.cells[c.x + c.y * self.width] = c
  end
end

function Map:import_entity_data(data)
  for i = 1,#data do
    local d = data[i]
    -- create new
    if self.entities[d.id] == nil then
      self.entities[d.id] = Entity:new{
        map_ref = self,
        entity_data_index = d.entity_data_index,
        id = d.id,
        pos = Vec2:new{x = d.x, y = d.y},
        state = d.s
      }
      if d.ts ~= nil then
        self.entities[d.id].target = {
          pos = Vec2:new{x = d.tx, y = d.ty},
          state = d.ts
        }
      end

      self.statistics.entity_n = self.statistics.entity_n + 1
    -- update existing
    else
      self.entities[d.id]:import_data(d)
    end
  end
end

function Map:import_object_data(data)
  for i = 1,#data do
    local d = data[i]
    -- create new
    if self.objects[d.id] == nil then
      self.objects[d.id] = Object:new{
        map_ref = self,
        object_data_index = d.object_data_index,
        id = d.id,
        pos = Vec2:new{x = d.x, y = d.y},
        custom_data = d.custom_data
      }

      self.statistics.object_n = self.statistics.object_n + 1
    -- update existing
    else
      self.objects[d.id]:import_data(d)
    end
  end
end

function Map:update(dt)
  -- run physics update
  if self.phys ~= nil then
    self.phys.world:update(dt)
  end

  -- clear screen data
  self.screen_cells = {}
  self.screen_entities = {}
  self.screen_objects = {}

  -- update cells and rebuild screen cells
  for _, c in pairs(self.cells) do
    if c ~= nil then
      -- cull if not on screen
      local w_cull, w_distance = IsoMath.cull_by_distance(Vec3:new{x = c.x, y = c.y, z = c.z}, self.client_ref.camera.w_pos, self.client_ref.config.render_distance)
      if w_cull == false then
        local screen_bbox = IsoMath.world_bbox_to_screen_bbox(c.bbox, c.z, 1.0, self.client_ref.camera.w_ofs.x, self.client_ref.camera.w_ofs.y, self.client_ref.camera.w_ofs.z)
        table.insert(self.screen_cells, {
          selected = false,
          distance = w_distance,
          bbox = screen_bbox,
          cell_ref = c
        })
      end
    end
  end

  -- update entities and rebuild screen entities
  for _, e in pairs(self.entities) do
    -- update entity
    e:update(dt)

    -- cull if not on screen
    local w_cull, w_distance = IsoMath.cull_by_distance(Vec3:new{x = e.pos.x, y = e.pos.y, z = e:get_pos_z()}, self.client_ref.camera.w_pos, self.client_ref.config.render_distance)
    if w_cull == false then
      local screen_bbox = IsoMath.world_bbox_to_screen_bbox(e.bbox, e:get_pos_z(), 0.5, self.client_ref.camera.w_ofs.x, self.client_ref.camera.w_ofs.y, self.client_ref.camera.w_ofs.z)
      table.insert(self.screen_entities, {
        selected = false,
        distance = w_distance,
        bbox = screen_bbox,
        entity_ref = e
      })
    end
  end

  -- update objects and rebuild screen objects
  for _, o in pairs(self.objects) do
    -- update object
    o:update(dt)

    -- cull if not on screen
    local w_cull, w_distance = IsoMath.cull_by_distance(Vec3:new{x = o.pos.x, y = o.pos.y, z = o:get_pos_z()}, self.client_ref.camera.w_pos, self.client_ref.config.render_distance)
    if w_cull == false then
      local screen_bbox = IsoMath.world_bbox_to_screen_bbox(o.bbox, o:get_pos_z(), 0.5, self.client_ref.camera.w_ofs.x, self.client_ref.camera.w_ofs.y, self.client_ref.camera.w_ofs.z)
      table.insert(self.screen_objects, {
        selected = false,
        distance = w_distance,
        bbox = screen_bbox,
        object_ref = o
      })
    end
  end
end

function Map:render()
  -- render screen cells
  for _, c in pairs(self.screen_cells) do
    -- determine tile alpha based on distance
    local alpha = 1.0
    if c.distance > self.client_ref.config.render_distance * self.client_ref.config.render_falloff then
      alpha = 1 - c.distance / self.client_ref.config.render_distance
    end
    -- render tiles
    for _, t in pairs(c.cell_ref.tiles) do
      local tileset = Tileset.load(t.tileset_index)
      tileset:draw_tile_iso(
        t.index,
        RGBA:new{r = 1, g = 1, b = 1, a = alpha},
        t.layer,
        Vec3:new{x = c.cell_ref.x, y = c.cell_ref.y, z = c.cell_ref.z},
        self.client_ref.camera.w_ofs
      )
    end
  end

  -- render screen entities
  for _, e in pairs(self.screen_entities) do
    -- determine entity alpha based on distance
    local alpha = 1.0
    if e.distance > self.client_ref.config.render_distance * self.client_ref.config.render_falloff then
      alpha = 1 - e.distance / self.client_ref.config.render_distance
    end
    e.entity_ref:render(alpha)
  end

  -- render screen objects
  for _, o in pairs(self.screen_objects) do
    -- determine object alpha based on distance
    local alpha = 1.0
    if o.distance > self.client_ref.config.render_distance * self.client_ref.config.render_falloff then
      alpha = 1 - o.distance / self.client_ref.config.render_distance
    end
    o.object_ref:render(alpha)
  end
end

function Map:render_debug()
  -- render screen cells
  if self.client_ref.config.render_debug.cells then
    love.graphics.setColor(255, 255, 255, 0.5)
    for _, c in pairs(self.screen_cells) do
      local render_mode = "line"
      if c.selected then
        render_mode = "fill"
      end
      love.graphics.rectangle(render_mode, c.bbox.min_x, c.bbox.min_y, c.bbox.dim_x, c.bbox.dim_y)
    end
  end

  -- render screen entities
  if self.client_ref.config.render_debug.entities then
    love.graphics.setColor(255, 0, 0, 0.5)
    for _, e in pairs(self.screen_entities) do
      local render_mode = "line"
      if e.selected then
        render_mode = "fill"
      end
      love.graphics.rectangle(render_mode, e.bbox.min_x, e.bbox.min_y, e.bbox.dim_x, e.bbox.dim_y)
    end
  end

  -- render screen objects
  if self.client_ref.config.render_debug.objects then
    love.graphics.setColor(0, 0, 255, 0.5)
    for _, o in pairs(self.screen_objects) do
      local render_mode = "line"
      if o.selected then
        render_mode = "fill"
      end
      love.graphics.rectangle(render_mode, o.bbox.min_x, o.bbox.min_y, o.bbox.dim_x, o.bbox.dim_y)
    end
  end
end

function Map:get_cell_by_xy(x, y)
  if x < 0 or x >= self.width or y < 0 or y >= self.height then return nil end

  x = math.floor(x)
  y = math.floor(y)

  local index = x + y * self.width
  return self.cells[index]
end

function Map:del_entity(id)
  if id == nil or self.entities[id] == nil then return end

  self.entities[id]:destroy()
  self.entities[id] = nil

  self.statistics.entity_n = self.statistics.entity_n - 1
end

function Map:get_screen_cell_by_screen_xy(x, y)
  local screen_cells = {}
  for _, c in pairs(self.screen_cells) do
    if c.bbox:intersect_point(x, y) ~= nil then
      c.selected = true
      table.insert(screen_cells, c)
    end
  end

  if #screen_cells > 0 then return screen_cells else return nil end
end

function Map:get_screen_entity_by_screen_xy(x, y)
  local screen_entities = {}
  for _, e in pairs(self.screen_entities) do
    if e.bbox:intersect_point(x, y) ~= nil then
      e.selected = true
      table.insert(screen_entities, e)
    end
  end

  if #screen_entities > 0 then return screen_entities else return nil end
end

function Map:get_screen_object_by_screen_xy(x, y)
  local screen_objects = {}
  for _, o in pairs(self.screen_objects) do
    if o.bbox:intersect_point(x, y) ~= nil then
      o.selected = true
      table.insert(screen_objects, o)
    end
  end

  if #screen_objects > 0 then return screen_objects else return nil end
end
