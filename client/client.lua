require "common.base"
require "common.protocol"
require "common.vec2"
require "common.vec3"
require "common.bbox"
require "graphics.iso_math"
require "client.packet"
require "client.connection"
require "client.map"
require "client.chat"
require "client.gui"
local socketlib = require "socket"
local json = require "3rdparty.json"

Client = setmetatable({
  class_name = "Client",
  tickrate = nil,
  socket = nil,
  connection = nil,
  ts_packet_out = nil,
  packets_out = nil,
  config = nil,
  map = nil,
  chat = nil,
  mouse = nil,
  camera = nil,
  gui = nil,
  state = nil
}, {__index = Base})

function Client:new(instance)
  instance = instance or {}
  setmetatable(instance, self)
  self.__index = self
  return instance:init()
end

function Client:init()
  self.tickrate = 1/30
  self.socket = socketlib.udp()
  self.socket:settimeout(0)
  self.socket:setpeername("127.0.0.1", 27050)
  self.connection = Connection:new{}
  self.ts_packet_out = nil
  self.packets_out = {}
  self.config = ObjUtil.set_if_nil(self.config, {
    render_debug = {
      cells = false,
      entities = true,
      objects = true
    },
    render_distance = 10.0,
    render_falloff = 0.6
  })
  self.map = Map:new{client_ref = self}
  self.chat = Chat:new{client_ref = self}
  self.mouse = ObjUtil.set_if_nil(self.mouse, {
    s_pos = Vec2:new{},
    w_pos = Vec3:new{},
    w_bbox = BBox:new{}
  })
  self.camera = ObjUtil.set_if_nil(self.camera, {
    w_pos = Vec3:new{},
    w_ofs = Vec3:new{}
  })
  self.gui = ObjUtil.set_if_nil(self.gui, GUI:new{
    client_ref = self
  })
  self.state = ObjUtil.set_if_nil(self.state, {
    latency_in_ms = 0.0,
    hovered_cell = nil,
    hovered_entity = nil,
    hovered_object = nil,
    entity_action = false,
    entity_action_timer = 0.0
  })

  -- send login request
  self:add_packet_out(Packet:new{
    id = PROTOCOL_PACKETS.LOGIN_REQ
  })

  return self
end

function Client:textinput(text)

end

function Client:keypressed(key, scancode, isrepeat)

end

function Client:keyreleased(key, scancode, isrepeat)

end

function Client:wheelmoved(x, y)
  local dx, dy = 0, 0
  if x > 0 then dx = 1 elseif x < 0 then dx = -1 end
  if y > 0 then dy = 1 elseif y < 0 then dy = -1 end
end

function Client:mousepressed(x, y, button, istouch, presses)
  if self.gui.hovered then return end

  -- entity action
  if button == 1 then
    self.state.entity_action = true
  end

  -- reset entity action timer
  self.state.entity_action_timer = 0.0
end

function Client:mousereleased(x, y, button, istouch, presses)
  -- entity reset action
  self.state.entity_action = false
end

function Client:mousemoved(x, y, dx, dy, istouch)
  if self.gui.hovered then return end

end

function Client:update(dt)
  -- process incoming packets
  self:process_packets_in()

  -- update map
  self.map:update(dt)

  -- update gui
  self.gui:update(dt)

  -- update mouse XY-coordinates
  self.mouse.s_pos.x, self.mouse.s_pos.y = love.mouse.getPosition()
  self.mouse.w_pos.x, self.mouse.w_pos.y, self.mouse.w_pos.z = IsoMath.screen_xy_to_world_xyz(self.mouse.s_pos.x, self.mouse.s_pos.y, self.camera.w_ofs.x, self.camera.w_ofs.y, self.camera.w_ofs.z)

  -- update mouse BBox-coordinates
  self.mouse.w_bbox.min_x = self.mouse.w_pos:floor().x
  self.mouse.w_bbox.min_y = self.mouse.w_pos:floor().y
  self.mouse.w_bbox.max_x = self.mouse.w_pos:floor().x + 1
  self.mouse.w_bbox.max_y = self.mouse.w_pos:floor().y + 1
  self.mouse.w_bbox:getPos()
  self.mouse.w_bbox:getDim()

  -- update game
  local player_entity = self.map.entities[self.connection.entity_id]
  if player_entity ~= nil then
    self.camera.w_pos.x = player_entity.pos.x
    self.camera.w_pos.y = player_entity.pos.y
    self.camera.w_pos.z = player_entity.render_vars.lerp_var_pos_z.val

    -- handle entity commands, rate limit with timer
    if self.state.entity_action_timer <= 0.0 and self.state.entity_action then
      player_entity:set_target_move(self.mouse.w_pos.x, self.mouse.w_pos.y)
      self:add_packet_out(Packet:new{
        id = PROTOCOL_PACKETS.ENTITY_MOVE_REQ,
        client_id = self.connection.id,
        token = self.connection.token,
        data = {
          id = player_entity.id,
          x = player_entity.target.pos.x,
          y = player_entity.target.pos.y
        }
      })
    end
    -- increment timer + reset at intervals
    self.state.entity_action_timer = self.state.entity_action_timer + dt
    if self.state.entity_action_timer >= 0.2 then
      self.state.entity_action_timer = 0.0
    end
  else
    self.camera.w_pos.x = self.map.width / 2
    self.camera.w_pos.y = self.map.height / 2
    self.camera.w_pos.z = 0
  end
  self.camera.w_ofs.x = -self.camera.w_pos.x
  self.camera.w_ofs.y = -self.camera.w_pos.y
  self.camera.w_ofs.z = -self.camera.w_pos.z

  self.map:get_screen_cell_by_screen_xy(self.mouse.s_pos.x, self.mouse.s_pos.y)
  self.map:get_screen_entity_by_screen_xy(self.mouse.s_pos.x, self.mouse.s_pos.y)
  self.map:get_screen_object_by_screen_xy(self.mouse.s_pos.x, self.mouse.s_pos.y)

  -- process outgoing packets
  self:process_packets_out()
end

function Client:render()
  -- render map
  self.map:render()

  -- render all isometric graphics
  IsoRender.execute_render_cmds()

  -- render map (debug)
  self.map:render_debug()

  -- render gui
  self.gui:render()
end

function Client:process_packets_in()
  -- check if any data, return if not, otherwise continue
  local data_str, ip, port = self.socket:receive()
  if not data_str then return end

  -- process connection data
  local data = json.decode(data_str)

  -- verify packet
  if data.id == nil then
    print('received invalid packet: ' .. data_str)
    return
  end
  local p_data = data.data

  print(data_str)

  -- pong request
  if data.id == PROTOCOL_PACKETS.PING then
    self:add_packet_out(Packet:new{
      id = PROTOCOL_PACKETS.PONG,
      client_id = self.connection.id,
      token = self.connection.token
    })
    self.state.latency_in_ms = p_data.latency_in_ms

  -- login response
  elseif data.id == PROTOCOL_PACKETS.LOGIN_RSP then
    self.connection = Connection:new{
      id = data.client_id,
      token = p_data.token,
      map_id = p_data.map_id,
      entity_id = p_data.entity_id
    }

  -- message response
  elseif data.id == PROTOCOL_PACKETS.MESSAGE_RSP then
    self.chat:add_message(p_data.source, p_data.sender, p_data.entity_id, p_data.message)

  -- map sync response
  elseif data.id == PROTOCOL_PACKETS.MAP_SYNC_RSP then
    self.map:import_data(p_data)

  -- cell sync response
  elseif data.id == PROTOCOL_PACKETS.CELL_SYNC_RSP then
    self.map:import_cell_data(p_data)

  -- entity sync response
  elseif data.id == PROTOCOL_PACKETS.ENTITY_SYNC_RSP then
    self.map:import_entity_data(p_data)

  -- entity delete response
  elseif data.id == PROTOCOL_PACKETS.ENTITY_DELETE_RSP then
    self.map:del_entity(p_data.id)

  -- entity move response
  elseif data.id == PROTOCOL_PACKETS.ENTITY_MOVE_RSP then
    local entity = self.map.entities[p_data.id]
    if entity ~= nil then
      entity:set_target_move(p_data.x, p_data.y)
    end

  -- object sync response
  elseif data.id == PROTOCOL_PACKETS.OBJECT_SYNC_RSP then
    self.map:import_object_data(p_data)
  end
end

function Client:add_packet_out(packet)
  if packet == nil or packet.id == nil then return end

  table.insert(self.packets_out, packet)
end

function Client:process_packets_out()
  -- get current timestamp
  local ts = socketlib.gettime()

  -- fixed tickrate
  if self.ts_packet_out ~= nil then
    local ts_diff = ts - self.ts_packet_out
    if ts_diff < self.tickrate then
      return
    end
  end

  for _, packet in ipairs(self.packets_out) do
    self.socket:send(json.encode(packet))
  end

  self.ts_packet_out = socketlib.gettime()
  self.packets_out = {}
end
