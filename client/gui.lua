require "common.base"
local Slab = require "3rdparty.slab.Slab"

GUI = setmetatable({
  class_name = "GUI",
  client_ref = nil,
  style = nil,
  hovered = nil
}, {__index = Base})

function GUI:new(instance)
  instance = instance or {}
  setmetatable(instance, self)
  self.__index = self
  return instance:init()
end

function GUI:init()
  Slab.Initialize()
  self.style = Slab.GetStyle()
  self.style.API.SetStyle("Default")
  self.hovered = false

  return self
end

function GUI:update(dt)
  Slab.Update(dt)

  -- draw stats
  Slab.BeginWindow("stats", {
    Title = "stats",
    TitleAlignX = "left",
    X = 0,
    Y = 0,
    AutoSizeWindow = true,
    NoSavedSettings = true
  })
  Slab.BeginLayout("stats-layout", {
    AlignX = "left",
    AlignY = "top"
  })
  Slab.Text(string.format("fps: %d", love.timer.getFPS()))
  Slab.Text(string.format("latency: %d ms", self.client_ref.state.latency_in_ms))
  Slab.Text(string.format("entity_n: %d", self.client_ref.map.statistics.entity_n))
  Slab.Text(string.format("object_n: %d", self.client_ref.map.statistics.object_n))
  Slab.EndLayout()
  Slab.EndWindow()

  -- draw chat
  Slab.BeginWindow("chat", {
    Title = "chat",
    TitleAlignX = "left",
    X = 128,
    Y = 128,
    W = 256,
    H = 128,
    AutoSizeWindow = false,
    NoSavedSettings = true
  })
  Slab.BeginLayout("chat-message-layout", {
    AlignX = "left",
    AlignY = "top"
  })
  -- print each message
  for i, v in ipairs(self.client_ref.chat.messages) do
    -- determine name
    local name = v.sender
    if name == nil then
      if v.source == MESSAGE_SOURCES.server then
        name = "SERVER"
      else
        name = "UNKNOWN"
      end
    end
    Slab.Text(string.format("%s: %s", name, v.message))
  end
  Slab.EndLayout()
  -- print chat input field
  Slab.BeginLayout("chat-input-layout", {
    AlignX = "left",
    AlignY = "bottom",
    ExpandW = true
  })
  Slab.Separator()
  if Slab.Input("chat-input", {Text = "", ReturnOnText = false}) then
    self.client_ref.chat:send_message(Slab.GetInputText())
  end
  Slab.EndLayout()
  Slab.EndWindow()

  -- update hovered
  self.hovered = Slab.IsVoidHovered() == false
end

function GUI:render()
  Slab.Draw()
end

function GUI:update_chat()

end
