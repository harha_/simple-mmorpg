require "common.base"

Packet = setmetatable({
  class_name = "Packet",
  id = nil,
  client_id = nil,
  token = nil,
  data = nil
}, {__index = Base})

function Packet:new(instance)
  instance = instance or {}
  setmetatable(instance, self)
  self.__index = self
  return instance:init()
end

function Packet:init()
  return self
end
