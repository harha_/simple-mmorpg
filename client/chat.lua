require "common.base"
require "common.message"
require "client.packet"
local json = require "3rdparty.json"

Chat = setmetatable({
  class_name = "Chat",
  client_ref = nil,
  buf_size = nil,
  messages = nil
}, {__index = Base})

function Chat:new(instance)
  instance = instance or {}
  setmetatable(instance, self)
  self.__index = self
  return instance:init()
end

function Chat:init()
  self.buf_size = ObjUtil.set_if_nil(self.buf_size, 10)
  self.messages = {}

  return self
end

function Chat:send_message(message)
  if message == nil then return end

  self.client_ref:add_packet_out(Packet:new{
    id = PROTOCOL_PACKETS.MESSAGE_REQ,
    client_id = self.client_ref.connection.id,
    token = self.client_ref.connection.token,
    data = {
      message = message
    }
  })
end

function Chat:add_message(source, sender, entity_id, message)
  if source == nil or message == nil then return end

  local messages_len = #self.messages
  if messages_len >= self.buf_size then
    table.remove(self.messages, 1)
  end

  table.insert(self.messages, {
    source = source,
    sender = sender,
    entity_id = entity_id,
    message = message
  })
end
