require "common.base"
require "graphics.animationsheet"
require "graphics.tileset"
require "graphics.spritesheet"
require "common.entity_data"
require "common.object_data"
require "client.client"

App = {
  client = nil
}

function App.load()
  -- setup window
  love.window.setMode(1280, 720, {resizable=false})
  love.window.setTitle("client")
  love.window.setFullscreen(false)
  love.window.setVSync(0)

  -- setup input
  love.keyboard.setKeyRepeat(true)

  -- load data
  Animationsheet.load_index()
  Tileset.load_index()
  Spritesheet.load_index()
  EntityData.load_index()
  ObjectData.load_index()

  -- load client
  App.client = Client:new{}
end

function love.textinput(text)
  App.client:textinput(text)
end

function love.keypressed(key, scancode, isrepeat)
  App.client:keypressed(key, scancode, isrepeat)
end

function love.keyreleased(key, scancode, isrepeat)
  App.client:keyreleased(key, scancode, isrepeat)
end

function love.wheelmoved(x, y)
  App.client:wheelmoved(x, y)
end

function love.mousepressed(x, y, button, istouch, presses)
  App.client:mousepressed(x, y, button, istouch, presses)
end

function love.mousereleased(x, y, button, istouch, presses)
  App.client:mousereleased(x, y, button, istouch, presses)
end

function love.mousemoved(x, y, dx, dy, istouch)
  App.client:mousemoved(x, y, dx, dy, istouch)
end

function love.update(dt)
  App.client:update(dt)
end

function love.draw()
  App.client:render()
end
