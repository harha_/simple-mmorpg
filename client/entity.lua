require "common.base"
require "common.entity"
require "common.entity_data"
require "common.vec2"
require "common.bbox"
require "common.math_util"
require "common.lerp_var"
require "graphics.spritesheet"
require "client.sprite"

Entity = setmetatable({
  class_name = "Entity",
  map_ref = nil,
  entity_data_index = nil,
  data = nil,
  sprite = nil,
  anim_time = nil,
  id = nil,
  pos = nil,
  bbox = nil,
  cell = nil,
  phys = nil,
  target = nil,
  state = nil,
  render_vars = nil
}, {__index = Base})

function Entity:new(instance)
  instance = instance or {}
  setmetatable(instance, self)
  self.__index = self
  return instance:init()
end

function Entity:init()
  self.entity_data_index = ObjUtil.set_if_nil(self.entity_data_index, 1)
  self.data = EntityData.load(self.entity_data_index)
  self.sprite = Sprite:new{
    spritesheet_index = self.data.spritesheet_indexes.idle,
    animation_index = 0,
    index = 0,
    layer = SPRITE_LAYERS.entity,
    color = RGBA:new{r = self.data.color.r, g = self.data.color.g, b = self.data.color.b, a = self.data.color.a}
  }
  self.anim_time = 0.0
  self.pos_z = 0.0
  self.bbox = ObjUtil.set_if_nil(self.bbox, BBox:new{})
  self:calculate_bbox()
  self.phys = nil
  self:init_phys()
  self.target = ObjUtil.set_if_nil(self.target, {
    pos = nil,
    state = nil
  })
  self.render_vars = ObjUtil.set_if_nil(self.render_vars, {
    lerp_var_pos_x = LerpVar:new{factor = 1.0},
    lerp_var_pos_y = LerpVar:new{factor = 1.0},
    lerp_var_pos_z = LerpVar:new{factor = 0.25}
  })

  print(string.format("loaded entity ID %d", self.id))

  return self
end

function Entity:init_phys()
  if self.phys ~= nil then
    self.phys.body:destroy()
    self.phys = nil
  end

  self.phys = {
    body = love.physics.newBody(self.map_ref.phys.world, self.pos.x, self.pos.y, "dynamic"),
    shape = love.physics.newCircleShape(self.data.size),
    fixture = nil
  }
  self.phys.fixture = love.physics.newFixture(self.phys.body, self.phys.shape, 1.0)
  self.phys.body:setLinearDamping(16.0)
end

function Entity:destroy()
  if self.phys ~= nil then
    self.phys.body:destroy()
    self.phys = nil
  end
end

function Entity:update(dt)
  -- update pos and bbox
  self.pos.x, self.pos.y = self.phys.body:getX(), self.phys.body:getY()
  self:calculate_bbox()

  -- update current cell
  self.cell = self.map_ref:get_cell_by_xy(self.pos.x, self.pos.y)

  -- linearly interpolate xyz-pos
  if self.render_vars.lerp_var_pos_x:is_active() then
    self.pos.x = self.render_vars.lerp_var_pos_x.val
  end
  if self.render_vars.lerp_var_pos_y:is_active() then
    self.pos.y = self.render_vars.lerp_var_pos_y.val
  end
  self.render_vars.lerp_var_pos_x:update(dt)
  self.render_vars.lerp_var_pos_y:update(dt)
  self.render_vars.lerp_var_pos_z:set_target(self.render_vars.lerp_var_pos_z.val, self:get_pos_z())
  self.render_vars.lerp_var_pos_z:update(dt)

  -- act according to current state
  if self.state == ENTITY_STATES.idle then
    -- pick a new target and state if exists
    if self.target.pos ~= nil and self.target.state ~= nil then
      self.state = self.target.state
    end
  elseif self.state == ENTITY_STATES.move then
    -- calc delta vector
    local delta = self.target.pos - self.pos
    local delta_norm = delta:normalize()
    -- move towards delta or go back to idle
    if delta:length() > 0.1 then
      -- update facing direction
      self:set_target_face(delta)
      -- apply physics force
      local delta_force = delta_norm:mul(self.data.speed)
      self.phys.body:applyForce(delta_force.x, delta_force.y)
    else
      self.target.pos = nil
      self.target.state = nil
      self.state = ENTITY_STATES.idle
    end
  end

  -- set sprite according to current state
  if self.state == ENTITY_STATES.idle then
    self.sprite.spritesheet_index = self.data.spritesheet_indexes.idle
  elseif self.state == ENTITY_STATES.move then
    self.sprite.spritesheet_index = self.data.spritesheet_indexes.move
  elseif self.state == ENTITY_STATES.melee then
    self.sprite.spritesheet_index = self.data.spritesheet_indexes.melee
  elseif self.state == ENTITY_STATES.magic then
    self.sprite.spritesheet_index = self.data.spritesheet_indexes.magic
  elseif self.state == ENTITY_STATES.dead then
    self.sprite.spritesheet_index = self.data.spritesheet_indexes.dead
  end
  self.anim_time = self.anim_time + dt
end

function Entity:render(alpha)
  -- load spritesheet
  local spritesheet = Spritesheet.load(self.sprite.spritesheet_index)

  -- load animation
  local anim = spritesheet.animationsheet.animations[self.sprite.animation_index]

  -- calculate animation frame
  local anim_frame = anim.frame_s_index + math.fmod(math.floor(self.anim_time * spritesheet.animationsheet.rate), anim.frame_e_index - anim.frame_s_index + 1)

  -- determine sprite color
  local sprite_color = RGBA:new{
    r = self.sprite.color.r,
    g = self.sprite.color.g,
    b = self.sprite.color.b,
    a = self.sprite.color.a * alpha
  }

  -- render the current frame
  spritesheet:draw_sprite_iso(
    anim_frame,
    sprite_color,
    self.sprite.layer,
    Vec3:new{
      x = self.pos.x,
      y = self.pos.y,
      z = self.render_vars.lerp_var_pos_z.val
    },
    self.map_ref.client_ref.camera.w_ofs
  )
end

function Entity:calculate_bbox()
  self.bbox.min_x = self.pos.x - self.data.size
  self.bbox.min_y = self.pos.y - self.data.size
  self.bbox.max_x = self.pos.x + self.data.size
  self.bbox.max_y = self.pos.y + self.data.size
  self.bbox:getPos()
  self.bbox:getDim()
end

function Entity:import_data(data)
  if self.entity_data_index ~= data.entity_data_index then
    self.entity_data_index = data.entity_data_index
    self.data = EntityData.load(self.entity_data_index)
    self.sprite = Sprite:new{
      spritesheet_index = self.data.spritesheet_indexes.idle,
      animation_index = 0,
      index = 0,
      layer = SPRITE_LAYERS.entity,
      color = RGBA:new{r = self.data.color.r, g = self.data.color.g, b = self.data.color.b, a = self.data.color.a}
    }
    self.anim_time = 0.0
  end

  self.phys.body:setX(data.x)
  self.phys.body:setY(data.y)
  if data.ts ~= nil then
    self.target.pos = Vec2:new{x = data.tx, y = data.ty}
    self.target.state = data.ts
  else
    self.target.pos = nil
    self.target.state = nil

    -- lerp client_pos->server_pos if too far apart
    local server_pos = Vec2:new{x = data.x, y = data.y}
    local pos_diff = (server_pos - self.pos):length()
    if pos_diff > 0.5 then
      self.render_vars.lerp_var_pos_x:set_target(self.pos.x, server_pos.x)
      self.render_vars.lerp_var_pos_y:set_target(self.pos.y, server_pos.y)
    end
  end
  self.state = data.s
end

function Entity:set_target_face(delta)
  local angle = delta:atan2() * 180.0 / math.pi - 22.5
  if angle < 0 then
    angle = 360 - math.abs(angle)
  end
  local angle_idx = math.min(math.floor(angle / 45), 7)
  self.sprite.animation_index = angle_idx
end

function Entity:set_target_move(x, y)
  self:init_target_move(x, y)
  self.target.pos = Vec2:new{x = x, y = y}
  self.target.state = ENTITY_STATES.move
end

function Entity:init_target_move(x, y)
  if self.target.state == ENTITY_STATES.move then return end

  self.anim_time = 0.0
end

function Entity:get_pos_z()
  if self.cell == nil then return 0 end

  return self.cell.z
end
