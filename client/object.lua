require "common.base"
require "common.object_data"
require "common.vec2"
require "common.bbox"
require "common.math_util"
require "graphics.spritesheet"
require "client.sprite"

Object = setmetatable({
  class_name = "Object",
  map_ref = nil,
  object_data_index = nil,
  data = nil,
  sprite = nil,
  anim_time = nil,
  id = nil,
  pos = nil,
  bbox = nil,
  cell = nil,
  custom_data = nil
}, {__index = Base})

function Object:new(instance)
  instance = instance or {}
  setmetatable(instance, self)
  self.__index = self
  return instance:init()
end

function Object:init()
  self.object_data_index = ObjUtil.set_if_nil(self.object_data_index, 1)
  self.data = ObjectData.load(self.object_data_index)
  self.sprite = Sprite:new{
    spritesheet_index = self.data.spritesheet_indexes.default,
    animation_index = 0,
    index = 0,
    layer = SPRITE_LAYERS.object,
    color = RGBA:new{r = self.data.color.r, g = self.data.color.g, b = self.data.color.b, a = self.data.color.a}
  }
  self.anim_time = 0.0
  self.bbox = ObjUtil.set_if_nil(self.bbox, BBox:new{})
  self:calculate_bbox()

  print(string.format("loaded object ID %d", self.id))

  return self
end

function Object:update(dt)
  -- update bbox
  self:calculate_bbox()

  -- update current cell
  self.cell = self.map_ref:get_cell_by_xy(self.pos.x, self.pos.y)

  self.anim_time = self.anim_time + dt
end

function Object:render(alpha)
  -- load spritesheet
  local spritesheet = Spritesheet.load(self.sprite.spritesheet_index)

  -- load animation
  local anim = spritesheet.animationsheet.animations[self.sprite.animation_index]

  -- calculate animation frame
  local anim_frame = anim.frame_s_index + math.fmod(math.floor(self.anim_time * spritesheet.animationsheet.rate), anim.frame_e_index - anim.frame_s_index + 1)

  -- determine sprite color
  local sprite_color = RGBA:new{
    r = self.sprite.color.r,
    g = self.sprite.color.g,
    b = self.sprite.color.b,
    a = self.sprite.color.a * alpha
  }

  -- render the current frame
  spritesheet:draw_sprite_iso(
    anim_frame,
    sprite_color,
    self.sprite.layer,
    Vec3:new{x = self.pos.x, y = self.pos.y, z = self:get_pos_z()},
    self.map_ref.client_ref.camera.w_ofs
  )
end

function Object:calculate_bbox()
  self.bbox.min_x = self.pos.x - self.data.size
  self.bbox.min_y = self.pos.y - self.data.size
  self.bbox.max_x = self.pos.x + self.data.size
  self.bbox.max_y = self.pos.y + self.data.size
  self.bbox:getPos()
  self.bbox:getDim()
end

function Object:import_data(data)
  if self.object_data_index ~= data.object_data_index then
    self.object_data_index = data.object_data_index
    self.data = ObjectData.load(self.object_data_index)
    self.sprite = Sprite:new{
      spritesheet_index = self.data.spritesheet_indexes.default,
      animation_index = 0,
      index = 0,
      layer = SPRITE_LAYERS.object,
      color = RGBA:new{r = self.data.color.r, g = self.data.color.g, b = self.data.color.b, a = self.data.color.a}
    }
    self.anim_time = 0.0
  end

  self.pos.x = data.x
  self.pos.y = data.y
  self.custom_data = data.custom_data
end

function Object:get_pos_z()
  if self.cell == nil then return 0 end

  return self.cell.z
end

