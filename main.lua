function love.load(args)
  -- parse cmd args
  for i, v in ipairs(args) do
    print("parse cmd arg, i: " .. i .. ", v: " .. v)

    -- split --key=value args
    local arg_key = nil
    local arg_val = nil
    for w in v:gmatch("([^=]+)") do
      if arg_key == nil then
        arg_key = w
      else
        arg_val = w
      end
    end

    -- launch map editor
    if arg_key == "--editor" then
      require "map_editor.app"
    -- launch server
    elseif arg_key == "--server" then
      require "server.app"
    -- launch client
    elseif arg_key == "--client" then
      require "client.app"
    -- launch client simulator
    elseif arg_key == "--client_sim" then
      require "client_sim.app"
    end
  end

  -- default: launch client
  if App == nil then
    require "client.app"
  end
  App.load()

  -- seed rng
  math.randomseed(os.time())
end
